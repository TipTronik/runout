﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using NLog;

namespace Runout
{
    public class Sensor
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();
        public int address;
        public double beating = 0;
        public Channel channel;
        public List<double> listData = new List<double>();

        private readonly int _sleepTimeout = 30;

        public Sensor(Channel channel, int address)
        {
            this.channel = channel;
            this.address = address;
        }

        public double ReciveData()
        {
            var rawDataBuff = new List<string>();
            var send = "";
            if (address < 10)
                send = string.Format("#0{0}LR" + "\r", address);
            if (address > 9)
                send = string.Format("#0{0}LR" + "\r", address.ToString("X1"));
            channel.serialPort.Write(send);
            Thread.Sleep(_sleepTimeout);
            rawDataBuff.Add(channel.serialPort.ReadExisting());

            if (rawDataBuff[0].Length < 12)
            {
                channel.serialPort.Write(send);
                Thread.Sleep(_sleepTimeout);
                rawDataBuff.Add(channel.serialPort.ReadExisting());
            }

            if (rawDataBuff[0].Length > 12) rawDataBuff = rawDataBuff[0].Split('\r').ToList();

            var pattern = ".*!(.*)LR(\\d{5})\r.*";

            var regex = new Regex(pattern);
            foreach (var rawData in rawDataBuff)
            {
                if (string.IsNullOrEmpty(rawData)) continue;
                var match = regex.Match(rawData);

                if (match.Success)
                    try
                    {
                        var a = int.Parse(match.Groups[1].Value, NumberStyles.HexNumber);
                        logger.Debug($"Data recived on sensor {address}: {rawData}");
                        if (int.Parse(match.Groups[1].Value, NumberStyles.HexNumber) == address)
                        {
                            if (double.Parse(match.Groups[2].Value) != 65535)
                            {
                                var value = 15.0 * double.Parse(match.Groups[2].Value) / 50000.0 + 20;

                                if (value > 0) return value;
                            }
                            else
                            {
                                return -1;
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        logger.Error($"Ошибка парсинга данных с датчика, строка от датчика: {rawData}", e);
                    }
                else
                    return -2;
            }

            return -3;
        }
    }
}