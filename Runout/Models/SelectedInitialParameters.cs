﻿using Runout.Models.Classes;

namespace Runout.Models
{
    public class SelectedInitialParameters
    {
        public static string kpImage { get; set; }
        public static Wheelset wheelset { get; set; }
        public static Operator currentOperator { get; set; }
        public static string kpNumber { get; set; }
    }
}