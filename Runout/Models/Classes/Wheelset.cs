﻿using System.Collections.Generic;

namespace Runout.Models.Classes
{
    public class Wheelset
    {
        public string Name { get; set; }
        public string Image { get; set; }
        public ICollection<Point> Points { get; set; } = new List<Point>();
        public double distance_out { get; set; }
        public double distance_in { get; set; }
        public bool peaks { get; set; }
        public List<bool> distrib_using { get; set; } = new List<bool>();
    }
}