﻿namespace Runout.Models.Classes
{
    public class Point
    {
        public virtual uint Id { get; set; }

        //public virtual Wheelset Wheelset { get; set; }
        public virtual int SensorAddress { get; set; }
        public virtual double RunoutLimit { get; set; }
        public virtual int PointNumber { get; set; }
        public virtual string Description { get; set; }
    }
}