﻿namespace Runout.Models.Classes
{
    public class Operator
    {
        public virtual uint Id { get; set; }
        public virtual string Name { get; set; }
    }
}