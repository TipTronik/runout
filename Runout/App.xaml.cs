﻿using System;
using System.Windows;
using MahApps.Metro;
using NLog;

namespace Runout
{
    /// <summary>
    ///     Interaction logic for App.xaml
    /// </summary>
    public partial class App
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        protected override void OnStartup(StartupEventArgs e)
        {
            logger.Info("Запуск приложения");
            ThemeManager.AddAccent("NiitkdTheme", new Uri("pack://application:,,,/Runout;component/NiitkdTheme.xaml"));
            var theme = ThemeManager.DetectAppStyle(Current);
            ThemeManager.ChangeAppStyle(Current, ThemeManager.GetAccent("NiitkdTheme"), theme.Item1);

            base.OnStartup(e);
        }
    }
}