﻿using System.Threading.Tasks;
using System.Windows;
using Runout.Utils;

namespace Runout.Views
{
    /// <summary>
    ///     Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        public MainWindow()
        {
            InitializeComponent();
            MainFrame.Navigate(new InitialParametesPage());

            //ComportWorks.Sensors_Off(new SerialPort("COM2", 115200), new SerialPort("COM1", 115200));
        }

        private async void InitialPositionBtn_OnClick(object sender, RoutedEventArgs e)
        {
            // mb disable all btns
            await ResetStand();
            MainFrame.Navigate(new InitialParametesPage());
        }

        private void SettingBtn_OnClick(object sender, RoutedEventArgs e)
        {
            MainFrame.Navigate(new SettingsPage());
        }

        private async void ExitBtn_OnClick(object sender, RoutedEventArgs e)
        {
            await ResetStand();
            Application.Current.Shutdown();
        }

        private async Task ResetStand()
        {
            await SiemensWork.toStartPosition();
        }
    }
}