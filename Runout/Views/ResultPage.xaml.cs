﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using iTextSharp.text;
using iTextSharp.text.pdf;
using NLog;
using Runout.Models;
using Runout.Models.Classes;
using Runout.Properties;
using Runout.Utils;
using Image = iTextSharp.text.Image;

namespace Runout.Views
{
    /// <summary>
    ///     Interaction logic for MeasurmentPage.xaml
    /// </summary>
    public partial class ResultPage : Page
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        public ResultPage()
        {
            InitializeComponent();
        }

        public static List<Sensor> result { get; set; } = new List<Sensor>();

        private void SaveReportBtn_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                if (SelectedInitialParameters.wheelset == null)
                    SelectedInitialParameters.wheelset = new Wheelset
                    {
                        Name = "Неизвестная КП"
                    };
                if (SelectedInitialParameters.currentOperator == null)
                    SelectedInitialParameters.currentOperator = new Operator
                    {
                        Name = "Неизвестный оператор"
                    };
                var directory = Settings.Default[@"reports_dir"] as string ?? @".\";
                if (!directory.EndsWith(@"\")) directory += '\\';
                directory += @"Runout";
                if (!Directory.Exists(directory)) Directory.CreateDirectory(directory);
                var filename = $@"{directory}\{DateTime.Now:yyyy_MM_dd_HH_mm_ss}.pdf";

                var font_base = BaseFont.CreateFont(@"Data\verdana.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
                var font_header = new Font(font_base, 22, Font.BOLD, new BaseColor(0, 0, 0));
                var font = new Font(font_base, 12, Font.NORMAL, new BaseColor(0, 0, 0));

                using (var stream =
                    new FileStream(filename, FileMode.Create, FileAccess.ReadWrite, FileShare.ReadWrite))
                using (var document = new Document(PageSize.A4, 25, 25, 30, 30))
                using (var writer = PdfWriter.GetInstance(document, stream))
                {
                    document.Open();

                    var header = new Paragraph("Отчет биения поверхностей колесных пар", font_header)
                    {
                        Alignment = Element.ALIGN_CENTER,
                        SpacingAfter = 20
                    };
                    document.Add(header);

                    var parameters = new PdfPTable(2);
                    parameters.AddCell(new Phrase("Тип колесной пары:", font));
                    parameters.AddCell(new Phrase($"{SelectedInitialParameters.wheelset.Name}", font));
                    parameters.AddCell(new Phrase("Номер колесной пары:", font));
                    parameters.AddCell(new Phrase($"{SelectedInitialParameters.kpNumber}", font));
                    parameters.AddCell(new Phrase("Оператор:", font));
                    parameters.AddCell(new Phrase($"{SelectedInitialParameters.currentOperator.Name}", font));
                    parameters.SpacingAfter = 20;
                    document.Add(parameters);

                    if (SelectedInitialParameters.kpImage != null)
                    {
                        var png = (BitmapImage)Application.Current.Resources[SelectedInitialParameters.kpImage];
                        byte[] data;
                        var encoder = new PngBitmapEncoder();
                        encoder.Frames.Add(BitmapFrame.Create(png));
                        using (var ms = new MemoryStream())
                        {
                            encoder.Save(ms);
                            data = ms.ToArray();
                        }

                        document.Add(Image.GetInstance(data));
                    }

                    var table = new PdfPTable(5) { SpacingBefore = 20, SpacingAfter = 20 };
                    table.AddCell(new PdfPCell());
                    table.AddCell(new Phrase("Описание", font));
                    table.AddCell(new Phrase("В пределах нормы", font));
                    table.AddCell(new Phrase("Фактическое значение", font));
                    table.AddCell(new Phrase("Предельное значение", font));

                    //for (var i = 0; i < result.Count; i++)
                    //{
                    //    try
                    //    {
                    //        var row = result.Where(r => r.address == i).FirstOrDefault();
                    //        if (row != null)
                    //        {
                    //            var point = SelectedInitialParameters.wheelset.Points.Where(p => p.SensorAddress == i)
                    //                .FirstOrDefault();
                    //            if (point != null)
                    //            {
                    //                table.AddCell($"{point.PointNumber}");
                    //                table.AddCell(new Phrase(point.Description, font));
                    //                table.AddCell(new Phrase(row.beating < point.RunoutLimit ? "+" : "-", font));
                    //                table.AddCell($"{Math.Round(row.beating, 2)}");
                    //                table.AddCell($"{point.RunoutLimit}");
                    //            }
                    //        }
                    //    }
                    //    catch (Exception inEx)
                    //    {
                    //        logger.Error(inEx);
                    //    }
                    //}

                    var wheelSet = SelectedInitialParameters.wheelset;
                    foreach (var point in wheelSet.Points)
                    {
                        try
                        {
                            var sensor = result.Where(r => r.address == point.SensorAddress).FirstOrDefault();

                            if (sensor != null)
                            {
                                if (point != null)
                                {
                                    table.AddCell($"{point.PointNumber}");
                                    table.AddCell(new Phrase(point.Description, font));
                                    table.AddCell(new Phrase(sensor.beating <= (point.RunoutLimit + 0.01) ? "+" : "-", font));
                                    table.AddCell($"{Math.Round(sensor.beating, 2)}");
                                    table.AddCell($"{point.RunoutLimit}");
                                }
                            }
                        }
                        catch (Exception inEx)
                        {
                            logger.Error(inEx);
                        }
                    }

                    document.Add(table);
                    var footer = new Paragraph($"{DateTime.Now:yyyy/MM/dd HH:mm:ss}", font)
                    {
                        Alignment = Element.ALIGN_RIGHT
                    };
                    document.Add(footer);
                    document.Close();
                }

                MessageBox.Show($"Отчет сформирован: {filename}");
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                MessageBox.Show(ex.Message);
            }
        }

        private void RedoBtn_OnClick(object sender, RoutedEventArgs e)
        {
            NavigationService?.Navigate(new StartMeasuringPage());
        }

        private void FinishBtn_OnClick(object sender, RoutedEventArgs e)
        {
            var task = Task.Run(SiemensWork.toStartPosition);
            NavigationService?.Navigate(new InProgressPage(new InitialParametesPage(), task));
        }

        private void ResultPage_OnLoaded(object sender, RoutedEventArgs e)
        {
            logger.Info("Show results:" + result.Count);
            DataContext = new ResultDataContext();
        }

        public class ResultRow
        {
            public ResultRow(int number, double factual_value, double max_value)
            {
                _number = number;
                inBounds = factual_value <= max_value + 0.01;
                factualValue = factual_value;
                maxValue = max_value;
            }

            public int _number { get; set; }
            public bool inBounds { get; set; }
            public double factualValue { get; set; }
            public double maxValue { get; set; }
        }

        public class ResultDataContext
        {
            public IList<ResultRow> Result
            {
                get
                {
                    var result_rows = new List<ResultRow>();
                    try
                    {
                        var wheelSet = SelectedInitialParameters.wheelset;

                        foreach (var point in wheelSet.Points)
                        {
                            var sensor = result.Where(r => r.address == point.SensorAddress).FirstOrDefault();

                            if (sensor != null)
                                result_rows.Add(new ResultRow(point.PointNumber, Math.Round(sensor.beating, 2),
                                    point.RunoutLimit));
                        }
                    }
                    catch (Exception ex)
                    {
                        logger.Error(ex);
                    }

                    return result_rows;
                }
            }
        }
    }
}