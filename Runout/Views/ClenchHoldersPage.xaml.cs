﻿using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using Runout.Utils;

namespace Runout.Views
{
    public partial class ClenchHoldersPage : Page
    {
        public ClenchHoldersPage()
        {
            InitializeComponent();
        }

        private void CancelBtn_OnClick(object sender, RoutedEventArgs e)
        {
            var task = Task.Run(() => SiemensWork.unclenchHolders());
            NavigationService?.Navigate(new InProgressPage(new InitialParametesPage(), task));
        }

        private void StartBtn_OnClick(object sender, RoutedEventArgs e)
        {
            var task = Task.Run(() => SiemensWork.pinchHolders());
            NavigationService?.Navigate(new InProgressPage(new StartMeasuringPage(), task));
        }
    }
}