﻿using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using Runout.Utils;

namespace Runout.Views
{
    /// <summary>
    ///     Interaction logic for InstallWheelsetPage.xaml
    /// </summary>
    public partial class PreliminaryLiftPage : Page
    {
        public PreliminaryLiftPage()
        {
            InitializeComponent();
        }

        private void PreliminaryLiftBtn_OnClick(object sender, RoutedEventArgs e)
        {
            var task = Task.Run(() => SiemensWork.preliminaryLift());
            NavigationService?.Navigate(new InProgressPage(new LiftOrLowerPage(), task));
        }
    }
}