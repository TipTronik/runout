﻿using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using Runout.Models;
using Runout.Utils;

namespace Runout.Views
{
    public partial class LiftOrLowerPage : Page
    {
        public LiftOrLowerPage()
        {
            InitializeComponent();
        }

        private void LowerBtn_OnClick(object sender, RoutedEventArgs e)
        {
            // Lower
            var task = Task.Run(() => SiemensWork.toStartPosition());
            NavigationService?.Navigate(new InProgressPage(new PreliminaryLiftPage(), task));
        }

        private void LiftBtn_OnClick(object sender, RoutedEventArgs e)
        {
            // todo::to start measuring
            var task = Task.Run(() => SiemensWork.liftToWorkPosition(SelectedInitialParameters.wheelset));
            NavigationService?.Navigate(new InProgressPage(new ClenchHoldersPage(), task));
        }
    }
}