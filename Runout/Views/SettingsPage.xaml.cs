﻿using System;
using System.Windows;
using System.Windows.Controls;
using Runout.Dao;
using Runout.Models.Classes;
using Runout.Properties;
using Runout.Utils;

namespace Runout.Views
{
    /// <summary>
    ///     Interaction logic for SettingsPage.xaml
    /// </summary>
    public partial class SettingsPage : Page
    {
        public SettingsPage()
        {
            InitializeComponent();
            DataContext = new ParamsDataContext();
        }

        private void addOperatorBtnClick(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(operatorNameTextBox.Text))
            {
                Repository<Operator>.Add(new Operator {Name = operatorNameTextBox.Text});
                operatorNameTextBox.Text = string.Empty;
                ;
                DataContext = new ParamsDataContext();
            }
        }

        private void deleteOperatorBtnClick(object sender, RoutedEventArgs e)
        {
            var id = Convert.ToUInt32(((Button) sender).CommandParameter);
            Repository<Operator>.Remove(id);
            DataContext = new ParamsDataContext();
        }

        private void backBtnClick(object sender, RoutedEventArgs e)
        {
            NavigationService?.Navigate(new InitialParametesPage());
        }

        private void Btn_setAverPeriod_OnClick(object sender, RoutedEventArgs e)
        {
            Settings.Default.AveragePeriod = int.Parse(txtBx_AveragePeriod.Text);
        }
    }
}