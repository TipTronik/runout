﻿using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using Runout.Utils;

namespace Runout.Views
{
    /// <summary>
    ///     Interaction logic for ParamsPage.xaml
    /// </summary>
    public partial class InitialParametesPage : Page
    {
        public InitialParametesPage()
        {
            InitializeComponent();
        }

        private void nextBtnClick(object sender, RoutedEventArgs e)
        {
            var task = Task.Run(() => SiemensWork.toStartPosition());
            NavigationService?.Navigate(new InProgressPage(new PreliminaryLiftPage(), task));
        }

        private void onLoad(object sender, RoutedEventArgs e)
        {
            DataContext = new ParamsDataContext();
            kpComboBox.SelectedIndex = 0;
        }
    }
}