﻿using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using Runout.Utils;

namespace Runout.Views
{
    public partial class StartMeasuringPage : Page
    {
        public StartMeasuringPage()
        {
            InitializeComponent();
        }

        private void CancelBtn_OnClick(object sender, RoutedEventArgs e)
        {
            var task = Task.Run(() => SiemensWork.toStartPosition());
            NavigationService?.Navigate(new InProgressPage(new InitialParametesPage(), task));
        }

        private void StartBtn_OnClick(object sender, RoutedEventArgs e)
        {
            var task = Task.Run(() => SiemensWork.startMeasurment());
            NavigationService?.Navigate(new InProgressPage(new ResultPage(), task));
        }
    }
}