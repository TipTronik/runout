﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using NLog;
using Runout.Utils;

namespace Runout.Views
{
    /// <summary>
    ///     Interaction logic for PreparationPage.xaml
    /// </summary>
    public partial class InProgressPage : Page
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();
        private readonly Page page;
        private readonly Task task;

        public InProgressPage(Page page, Task task)
        {
            InitializeComponent();
            this.task = task;
            this.page = page;
        }

        private void continueBtnClick(object sender, RoutedEventArgs e)
        {
            NavigationService?.Navigate(page);
        }

        private async void Page_Loaded(object sender, RoutedEventArgs e)
        {
            if (page is ResultPage)
                this.StatusLabel.Content = "Идет Измерение";

            await task;

            if (page is ResultPage)
                try
                {
                    var result = (task as Task<List<Sensor>>)?.Result;
                    logger.Info("Result count: " + result.Count);
                    ResultPage.result = result;
                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                }

            continueBtn.IsEnabled = task.IsCompleted;
        }

        private void BackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            //SiemensWork.toStartPosition();
        }

        private void BackgroundWorker_Completed(object sender, RunWorkerCompletedEventArgs e)
        {
            continueBtn.IsEnabled = true;
        }

        private async void backBtnClick(object sender, RoutedEventArgs e)
        {
            // mb disable all btns
            await SiemensWork.toStartPosition();
            //MainFrame.Navigate(new InitialParametesPage());
            NavigationService?.Navigate(new InitialParametesPage());
        }
    }
}