﻿using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Linq;
using Runout.Models.Classes;

namespace Runout.Dao
{
    public static class Repository<T>
    {
        private static readonly List<Wheelset> wheelsets = new List<Wheelset>();

        public static IList<T> All()
        {
            wheelsets.Clear();
            Database.InitializeWheelsets();
            if (typeof(T) == typeof(Wheelset)) return wheelsets as IList<T>;
            using (var session = Database.SESSION_FACTORY.OpenSession())
            {
                return session.Query<T>().ToList();
            }
        }

        public static T First()
        {
            using (var session = Database.SESSION_FACTORY.OpenSession())
            {
                return session.Query<T>().First();
            }
        }

        public static void Add(T item)
        {
            if (item is Wheelset)
                wheelsets.Add(item as Wheelset);
            else
                using (var session = Database.SESSION_FACTORY.OpenSession())
                {
                    var existing = GetByExample(session, item);
                    if (existing == null || existing.Count == 0) session.SaveOrUpdate(item);
                    session.Flush();
                }
        }

        public static void Remove(uint id)
        {
            using (var session = Database.SESSION_FACTORY.OpenSession())
            {
                session.Delete(session.Load<T>(id));
                session.Flush();
            }
        }

        private static IList<C> GetByExample<C>(ISession session, C example)
        {
            return session.CreateCriteria(typeof(C))
                .Add(Example.Create(example))
                .List<C>();
        }
    }
}