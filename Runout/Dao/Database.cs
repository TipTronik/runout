﻿using System.Collections.Generic;
using System.IO;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;
using Runout.Models.Classes;

namespace Runout.Dao
{
    internal static class Database
    {
        public static readonly ISessionFactory SESSION_FACTORY;

        static Database()
        {
            //InitializeWheelsets();

            var cfg = new Configuration();
            cfg.Configure();
            cfg.AddAssembly(typeof(Wheelset).Assembly);
            if (!File.Exists("runout.db"))
            {
                var schema = new SchemaExport(cfg);
                schema.Create(true, true);
            }

            SESSION_FACTORY = cfg.BuildSessionFactory();
        }

        public static void InitializeWheelsets()
        {
            var wheelsets = new Dictionary<string, string>
            {
                {"81-714/717 (МОТОРНАЯ)", "714_717_black"},
                //{"81-720/721", "720_721_M_black"},
                {"81-740/741 (МОТОРНАЯ)", "740_741_M_black"},
                {"81-740/741 (НЕ МОТОРНАЯ)", "740_741_NM_black"}
            };

            foreach (var wheelset in wheelsets)
            {
                var w = new Wheelset {Name = wheelset.Key, Image = wheelset.Value};
                AddPoints(w);
                Repository<Wheelset>.Add(w);
            }
        }

        private static void AddPoints(Wheelset wheelset)
        {
            switch (wheelset.Name)
            {
                case "81-714/717 (МОТОРНАЯ)":
                    wheelset.peaks = true;
                    wheelset.distance_out = 30;
                    wheelset.distance_in = 29;
                    wheelset.distrib_using.Add(true);
                    wheelset.distrib_using.Add(true);
                    wheelset.distrib_using.Add(true);
                    wheelset.distrib_using.Add(true);
                    wheelset.distrib_using.Add(true);

                    wheelset.Points.Add(new Point
                    {
                        PointNumber = 1,
                        SensorAddress = 1,
                        RunoutLimit = 0.04,
                        Description = "Радиальное биение шеек оси"
                    });
                    wheelset.Points.Add(new Point
                    {
                        PointNumber = 2,
                        SensorAddress = 2,
                        RunoutLimit = 0.04,
                        Description = "Радиальное биение предподступичных частей"
                    });
                    wheelset.Points.Add(new Point
                    {
                        PointNumber = 3,
                        SensorAddress = 3,
                        RunoutLimit = 0.5,
                        Description = "Радиальное биение поверхности катания колес"
                    });
                    wheelset.Points.Add(new Point
                    {
                        PointNumber = 4,
                        SensorAddress = 4,
                        RunoutLimit = 0.2,
                        Description = "Торцевое биение колеса"
                    });
                    wheelset.Points.Add(new Point
                    {
                        PointNumber = 5,
                        SensorAddress = 5,
                        RunoutLimit = 0.6,
                        Description = "Радиальное биение средней части оси"
                    });
                    wheelset.Points.Add(new Point
                    {
                        PointNumber = 6,
                        SensorAddress = 6,
                        RunoutLimit = 0.2
                    });
                    wheelset.Points.Add(new Point
                    {
                        PointNumber = 7,
                        SensorAddress = 7,
                        RunoutLimit = 0.2
                    });
                    wheelset.Points.Add(new Point
                    {
                        PointNumber = 8,
                        SensorAddress = 8,
                        RunoutLimit = 0.2
                    });
                    wheelset.Points.Add(new Point
                    {
                        PointNumber = 9,
                        SensorAddress = 9,
                        RunoutLimit = 0.2,
                        Description = "Торцевое биение колеса"
                    });
                    wheelset.Points.Add(new Point
                    {
                        PointNumber = 10,
                        SensorAddress = 10,
                        RunoutLimit = 0.5,
                        Description = "Радиальное биение поверхности катания колес"
                    });
                    wheelset.Points.Add(new Point
                    {
                        PointNumber = 11,
                        SensorAddress = 11,
                        RunoutLimit = 0.04,
                        Description = "Радиальное биение предподступичных частей"
                    });
                    wheelset.Points.Add(new Point
                    {
                        PointNumber = 12,
                        SensorAddress = 12,
                        RunoutLimit = 0.04,
                        Description = "Радиальное биение шеек оси"
                    });
                    break;
                case "81-720/721":
                    wheelset.distance_out = 31;
                    wheelset.distance_in = 33;
                    wheelset.peaks = false;

                    wheelset.distrib_using.Add(true);
                    wheelset.distrib_using.Add(true);
                    wheelset.distrib_using.Add(false);
                    wheelset.distrib_using.Add(true);
                    wheelset.distrib_using.Add(false);

                    wheelset.Points.Add(new Point
                    {
                        PointNumber = 1,
                        SensorAddress = 1,
                        RunoutLimit = 0.04,
                        Description = "Радиальное биение шеек оси"
                    });
                    wheelset.Points.Add(new Point
                    {
                        PointNumber = 2,
                        SensorAddress = 2,
                        RunoutLimit = 0.04,
                        Description = "Радиальное биение предподступичных частей"
                    });
                    wheelset.Points.Add(new Point
                    {
                        PointNumber = 3,
                        SensorAddress = 3,
                        RunoutLimit = 0.5,
                        Description = "Радиальное биение поверхности катания колес"
                    });
                    wheelset.Points.Add(new Point
                    {
                        PointNumber = 4,
                        SensorAddress = 4,
                        RunoutLimit = 0.2,
                        Description = "Торцевое биение колеса"
                    });
                    wheelset.Points.Add(new Point
                    {
                        PointNumber = 5,
                        SensorAddress = 9,
                        RunoutLimit = 0.2,
                        Description = "Торцевое биение колеса"
                    });
                    wheelset.Points.Add(new Point
                    {
                        PointNumber = 6,
                        SensorAddress = 10,
                        RunoutLimit = 0.5,
                        Description = "Радиальное биение поверхности катания колес"
                    });
                    wheelset.Points.Add(new Point
                    {
                        PointNumber = 7,
                        SensorAddress = 11,
                        RunoutLimit = 0.04,
                        Description = "Радиальное биение предподступичных частей"
                    });
                    wheelset.Points.Add(new Point
                    {
                        PointNumber = 8,
                        SensorAddress = 12,
                        RunoutLimit = 0.04,
                        Description = "Радиальное биение шеек оси"
                    });
                    break;
                case "81-740/741 (МОТОРНАЯ)":
                    wheelset.distance_out = 24;
                    wheelset.distance_in = 26;
                    wheelset.peaks = false;

                    wheelset.distrib_using.Add(true);
                    wheelset.distrib_using.Add(true);
                    wheelset.distrib_using.Add(false);
                    wheelset.distrib_using.Add(true);
                    wheelset.distrib_using.Add(false);

                    wheelset.Points.Add(new Point
                    {
                        PointNumber = 1,
                        SensorAddress = 1,
                        RunoutLimit = 0.04,
                        Description = "Радиальное биение шеек оси"
                    });
                    wheelset.Points.Add(new Point
                    {
                        PointNumber = 2,
                        SensorAddress = 2,
                        RunoutLimit = 0.04,
                        Description = "Радиальное биение предподступичных частей"
                    });
                    wheelset.Points.Add(new Point
                    {
                        PointNumber = 3,
                        SensorAddress = 3,
                        RunoutLimit = 0.5,
                        Description = "Радиальное биение поверхности катания колес"
                    });
                    wheelset.Points.Add(new Point
                    {
                        PointNumber = 4,
                        SensorAddress = 4,
                        RunoutLimit = 0.2,
                        Description = "Торцевое биение колеса"
                    });
                    wheelset.Points.Add(new Point
                    {
                        PointNumber = 5,
                        SensorAddress = 9,
                        RunoutLimit = 0.2,
                        Description = "Торцевое биение колеса"
                    });
                    wheelset.Points.Add(new Point
                    {
                        PointNumber = 6,
                        SensorAddress = 10,
                        RunoutLimit = 0.5,
                        Description = "Радиальное биение поверхности катания колес"
                    });
                    wheelset.Points.Add(new Point
                    {
                        PointNumber = 7,
                        SensorAddress = 11,
                        RunoutLimit = 0.04,
                        Description = "Радиальное биение предподступичных частей"
                    });
                    wheelset.Points.Add(new Point
                    {
                        PointNumber = 8,
                        SensorAddress = 12,
                        RunoutLimit = 0.04,
                        Description = "Радиальное биение шеек оси"
                    });
                    break;
                case "81-740/741 (НЕ МОТОРНАЯ)":
                    wheelset.distance_out = 24;
                    wheelset.distance_in = 26;
                    wheelset.peaks = false;

                    wheelset.distrib_using.Add(true);
                    wheelset.distrib_using.Add(true);
                    wheelset.distrib_using.Add(false);
                    wheelset.distrib_using.Add(true);
                    wheelset.distrib_using.Add(false);

                    wheelset.Points.Add(new Point
                    {
                        PointNumber = 1,
                        SensorAddress = 1,
                        RunoutLimit = 0.04,
                        Description = "Радиальное биение шеек оси"
                    });
                    wheelset.Points.Add(new Point
                    {
                        PointNumber = 2,
                        SensorAddress = 2,
                        RunoutLimit = 0.04,
                        Description = "Радиальное биение предподступичных частей"
                    });
                    wheelset.Points.Add(new Point
                    {
                        PointNumber = 3,
                        SensorAddress = 3,
                        RunoutLimit = 0.5,
                        Description = "Радиальное биение поверхности катания колес"
                    });
                    wheelset.Points.Add(new Point
                    {
                        PointNumber = 4,
                        SensorAddress = 4,
                        RunoutLimit = 0.2,
                        Description = "Торцевое биение колеса"
                    });
                    wheelset.Points.Add(new Point
                    {
                        PointNumber = 5,
                        SensorAddress = 8,
                        RunoutLimit = 0.6,
                        Description = "Радиальное биение средней части оси"
                    });
                    wheelset.Points.Add(new Point
                    {
                        PointNumber = 6,
                        SensorAddress = 9,
                        RunoutLimit = 0.2,
                        Description = "Торцевое биение колеса"
                    });
                    wheelset.Points.Add(new Point
                    {
                        PointNumber = 7,
                        SensorAddress = 10,
                        RunoutLimit = 0.5,
                        Description = "Радиальное биение поверхности катания колес"
                    });
                    wheelset.Points.Add(new Point
                    {
                        PointNumber = 8,
                        SensorAddress = 11,
                        RunoutLimit = 0.04,
                        Description = "Радиальное биение предподступичных частей"
                    });
                    wheelset.Points.Add(new Point
                    {
                        PointNumber = 9,
                        SensorAddress = 12,
                        RunoutLimit = 0.04,
                        Description = "Радиальное биение шеек оси"
                    });
                    break;
            }
        }
    }
}