﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows.Data;

namespace Runout.Utils
{
    public sealed class MinConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            return System.Convert.ToBoolean(values.Cast<int>().Min());
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}