﻿using System;
using System.Collections.Generic;

namespace Runout
{
    public static class Analyzer
    {
        public static double[] MovingAverage(double[] data, int delta)
        {
            var result = new double[data.Length];
            for (var i = 0; i < data.Length; i++)
            {
                if (i < delta)
                {
                    for (var j = 0; j < i + delta; j++) result[i] += data[j];
                    result[i] /= i + delta;
                }

                if (i >= delta && i <= data.Length - delta)
                {
                    for (var j = i - delta; j < i + delta; j++) result[i] += data[j];
                    result[i] /= delta + delta;
                }

                if (i > data.Length - delta)
                {
                    for (var j = i - delta; j < data.Length; j++) result[i] += data[j];
                    result[i] /= delta + data.Length - i;
                }
            }

            return result;
        }

        private static double MedianFilter(List<double> buffer)
        {
            var window = 9;
            if (buffer.Count == window)
            {
                var sortBuffer = new List<double>(buffer);
                sortBuffer.Sort();
                return sortBuffer[window / 2 + 1];
            }

            if (buffer.Count > window)
            {
                buffer.RemoveAt(0);
                var sortBuffer = new List<double>(buffer);
                sortBuffer.Sort();
                return sortBuffer[window / 2 + 1];
            }

            if (buffer.Count < window)
            {
                var sortBuffer = new List<double>(buffer);
                sortBuffer.Sort();
                return sortBuffer[sortBuffer.Count / 2];
            }

            return 0;
        }

        public static double Beating(List<double> data)
        {
            if (data != null && data.Count > 0)
            {
                data.Sort();
                return data[data.Count - 1] - data[0];
            }

            return 0;
        }

        public static List<double> FindPeak(List<double> data)
        {
            var bufRes = new List<double>();
            for (var i = 1; i < data.Count - 1; ++i)
                if (Math.Abs(data[i] - data[i - 1]) < 0.05)
                    bufRes.Add(data[i]);
            var result = new List<double>();

            for (var i = 1; i < bufRes.Count - 1; ++i)
                if (Math.Abs(bufRes[i] - bufRes[i - 1]) < 0.05)
                    result.Add(bufRes[i]);
            return result;
        }
    }
}