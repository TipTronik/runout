﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using NLog;
using Runout.Models;
using Runout.Models.Classes;
using S7.Net;

namespace Runout.Utils
{
    public static class SiemensWork
    {
        private static readonly string deviceIpAddress = "192.168.0.222";
        private static readonly short rackNumber = 0;
        private static readonly short slotNumber = 1;
        private static readonly Plc plc = new Plc(CpuType.S71200, deviceIpAddress, rackNumber, slotNumber);
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        public static void liftersUp()
        {
            left_lifter_up();
            right_lifter_up();
        }

        /// <summary>
        /// </summary>
        /// <returns></returns>
        public static async Task toStartPosition() //step 1
        {
            try
            {
                logger.Info("Начало движения в стартовое положение");
                kp_stop();
                ComportWorks.Sensors_Off();
                p1_unclenchPenola();
                Thread.Sleep(4000);
                all_P(false);
                left_lifter_null();
                right_lifter_null();
            }
            catch (Exception e)
            {
                logger.Error(e);
            }
        }

        /// <summary>
        ///     Предварительный подъем
        /// </summary>
        /// <returns></returns>
        public static async Task preliminaryLift() //step 2
        {
            try
            {
                logger.Info("Начало предварительного подъема");
                left_lifter_up();
                right_lifter_up();
                Thread.Sleep(15000);
                left_lifter_stop();
                right_lifter_stop();
            }
            catch (Exception e)
            {
                logger.Error(e);
                toStartPosition();
            }
        }

        public static async Task liftToWorkPosition(Wheelset wheelSet) //step 3
        {
            try
            {
                logger.Info("Начало движения в рабочее положение");
                ComportWorks.Sensors_On();
                logger.Info("Сенсоры включены");

                ComportWorks.OpenPorts();
                logger.Info("Ком-порты открыты");

                var sensor_1 = new Sensor(new Channel(ComportWorks.serialPort2, 1), 1);
                var sensor_2 = new Sensor(new Channel(ComportWorks.serialPort2, 1), 2);
                var sensor_11 = new Sensor(new Channel(ComportWorks.serialPort1, 2), 11);
                var sensor_12 = new Sensor(new Channel(ComportWorks.serialPort1, 2), 12);

                p2(wheelSet.distrib_using[1]);
                p3(wheelSet.distrib_using[2]);
                p4(wheelSet.distrib_using[3]);
                p5(wheelSet.distrib_using[4]);

                //var lift = true;
                var lift_right = true;
                var lift_left = true;

                //var interval0_10 = true;
                var distance_out = wheelSet.distance_out;
                var distance_in = wheelSet.distance_in;

                var left_out = -1.0;
                var left_in = -1.0;
                var right_out = -1.0;
                var right_in = -1.0;
                left_lifter_up();
                right_lifter_up();
                while (lift_left || lift_right)
                {
                    left_out = sensor_1.ReciveData();
                    right_out = sensor_12.ReciveData();
                    left_in = sensor_2.ReciveData();
                    right_in = sensor_11.ReciveData();

                    if (left_out == -2 && right_out == -2 && left_in == -2 && right_in == -2)
                    {
                        lifters_stop();
                        all_P(false);
                    }

                    if (0 <= left_out && left_out <= distance_out && 0 <= left_in && left_in <= distance_in)
                    {
                        left_lifter_stop();
                        lift_left = false;
                    }

                    if (0 <= right_out && right_out <= distance_out && 0 <= right_in && right_in <= distance_in)
                    {
                        right_lifter_stop();
                        lift_right = false;
                    }
                    //if (interval0_10 && right_data > 20 && right_data < 36.5)
                    //{
                    //    MessageBox.Show($"{right_data}");
                    //    interval0_10 = false;
                    //}

                    //if (measureSensor_Right.ReciveData() <= 31 /* && measureSensor_Right.ReciveData() < 1*/)
                    //{
                    //    lift_right = false;

                    //    right_lifter_stop();
                    //}
                    //if (measureSensor_Left.ReciveData() <= 31 /*&& measureSensor_Left.ReciveData() < 1*/)
                    //{
                    //    lift_left = false;

                    //    left_lifter_stop();
                    //}

                    //if (lift_left == false && lift_right == false)
                    //{
                    //    lift = false;
                    //}
                }

                lifters_stop();

                left_out = sensor_1.ReciveData();
                right_out = sensor_12.ReciveData();

                var e = 1.0;

                while (!(distance_out - e <= left_out && left_out <= distance_out + e) ||
                       !(distance_out - e <= right_out && right_out <= distance_out + e))
                {
                    left_out = sensor_1.ReciveData();
                    right_out = sensor_12.ReciveData();

                    if (left_out < 0 || right_out < 0)
                    {
                        lifters_stop();
                        break;
                    }

                    if (distance_out - e <= left_out && left_out <= distance_out + e) left_lifter_stop();
                    if (distance_out - e <= right_out && right_out <= distance_out + e) right_lifter_stop();
                    if (left_out < distance_out - e) left_lifter_null();
                    if (right_out < distance_out - e) right_lifter_null();
                    if (distance_out + e < left_out) left_lifter_up();
                    if (distance_out + e < right_out) right_lifter_up();
                }
            }
            catch (Exception ex)
            {
                lifters_stop();
                logger.Error(ex);
                throw;
            }
            finally
            {
                ComportWorks.ClosePorts();
                logger.Info("Порты закрыты");
            }
        }

        public static async Task pinchHolders()
        {
            try
            {
                logger.Info("Зажать пиноли");
                p1_pinchPenola();
                Thread.Sleep(5000);
                //liftersUp();
                //Thread.Sleep(300);
                //lifters_stop();
            }
            catch (Exception e)
            {
                logger.Error(e);
            }
        }

        public static async Task unclenchHolders()
        {
            try
            {
                logger.Info("Разжать пиноли");
                p1_unclenchPenola();
                Thread.Sleep(5000);
            }
            catch (Exception e)
            {
                logger.Error(e);
            }
        }

        public static async Task<List<Sensor>> startMeasurment()
        {
            logger.Info("Начать измерение");
            kp_start();
            Thread.Sleep(10000);
            var sensors = new List<Sensor>();
            //
            try
            {
                ComportWorks.Sensors_On();

                ComportWorks.OpenPorts();

                var chan1 = new Channel(ComportWorks.serialPort2, 1);
                var chan2 = new Channel(ComportWorks.serialPort1, 2);

                for (var i = 1; i <= 12; ++i)
                {
                    if (i <= 4) sensors.Add(new Sensor(chan1, i));
                    if (i > 4) sensors.Add(new Sensor(chan2, i));
                }

                var j = 0;
                while (j < 200)
                {
                    logger.Info("Запрос данных");
                    var send = "#00FX" + "\r";
                    chan1.serialPort.Write(send);
                    chan2.serialPort.Write(send);
                    Thread.Sleep(50);

                    foreach (var sensor in sensors)
                    {
                        var reciveData = sensor.ReciveData();
                        if (reciveData > 0)
                        {
                            logger.Info($"Данные получены {sensor.channel.channel}:{sensor.address}:" + reciveData);
                            sensor.listData.Add(reciveData);
                        }

                        if (j == 199)
                        {
                            if (sensor.listData.Count > 0)
                            {
                                logger.Info("Данные для анализа:" + sensor.listData.Count);
                                var average = Analyzer.MovingAverage(sensor.listData.ToArray(), 10);
                                sensor.beating = Analyzer.Beating(average.ToList());
                                logger.Info("Данные анализа" + sensor.beating);
                            }
                            else
                            {
                                logger.Error($"{sensor.address}: Нет данных для анализа");
                            }
                        }
                    }

                    ++j;
                }

                if (SelectedInitialParameters.wheelset.peaks)
                {
                    sensors[4].listData.Clear();
                    changeSpeed(500);
                    Thread.Sleep(3000);
                    for (var i = 0; i < 3000;)
                    {
                        var send = "#00FX" + "\r";
                        chan1.serialPort.Write(send);
                        Thread.Sleep(5);
                        var reciveData = sensors[4].ReciveData();
                        if (reciveData > 0)
                        {
                            sensors[4].listData.Add(reciveData);
                            ++i;
                            if (i == 2999)
                            {
                                var peaks = Analyzer.FindPeak(sensors[4].listData);
                                var average = Analyzer.MovingAverage(peaks.ToArray(), 10);
                                sensors[4].beating = Analyzer.Beating(average.ToList());
                                //using (var w = new StreamWriter("Sensor5data.txt", false))
                                //{
                                //    sensors[4].listData.ForEach(d => w.WriteLine(d));
                                //}
                            }
                        }
                    }
                }

                logger.Info($"Count of result: {sensors.Count}");
            }
            catch (Exception e)
            {
                logger.Error(e);
                logger.Debug(e);
            }
            finally
            {
                ComportWorks.ClosePorts();
                kp_stop();
            }

            return sensors;
        }

        public static async Task CompleteMeasurmentTask() //step 6
        {
            logger.Info("Завершение диагностики");
            //выкл датчики
            kp_stop();
            Thread.Sleep(10000);
            all_P(false);
            p1_unclenchPenola();
            Thread.Sleep(10000);
            lifters_null();
        }

        public static void changeSpeed(int value)
        {
            logger.Info("Изменение скорости:" + value);
            plc.Open();
            if (plc.IsConnected) plc.Write("DB3.DBW2", value);
        }

        public static void kp_start()
        {
            logger.Info("Начало вращения КП");
            plc.Open();
            if (plc.IsConnected)
            {
                var val = 900;
                plc.Write("DB3.DBW2", val);
                var d = Convert.ToDouble(plc.Read("DB3.DBD2"));
                plc.Write("DB3.DBX0.0", 1);
                //Get data
            }
        }

        public static void kp_stop()
        {
            logger.Info("Остановка вращения кп");
            plc.Open();
            if (plc.IsConnected) plc.Write("DB3.DBX0.2", 1);
        }

        public static void lifters_stop()
        {
            left_lifter_stop();
            right_lifter_stop();
            logger.Info("Остановка домкратов");
        }

        public static void lifters_null()
        {
            logger.Info("Домкраты в нулевое положение");
            left_lifter_null();
            right_lifter_null();
        }

        private static void left_lifter_up()
        {
            logger.Info("Левый домкрат вверх");
            plc.Open();
            if (plc.IsConnected) plc.Write("DB2.DBX0.0", 1);
        }

        private static void right_lifter_up()
        {
            logger.Info("Правый домкрат вверх");
            plc.Open();
            if (plc.IsConnected) plc.Write("DB4.DBX0.0", 1);
        }

        private static void left_lifter_stop()
        {
            logger.Info("Левый домкрат стоп");
            plc.Open();
            if (plc.IsConnected) plc.Write("DB2.DBX0.2", 1);
        }

        private static void right_lifter_stop()
        {
            logger.Info("Правый домкрат стоп");
            plc.Open();
            if (plc.IsConnected) plc.Write("DB4.DBX0.2", 1);
        }

        private static void left_lifter_null()
        {
            plc.Open();
            if (plc.IsConnected) plc.Write("DB2.DBX0.3", 1);
        }

        private static void right_lifter_null()
        {
            plc.Open();
            if (plc.IsConnected) plc.Write("DB4.DBX0.3", 1);
        }

        public static void p1_pinchPenola()
        {
            logger.Info("Зажать пиноли");
            plc.Open();
            if (plc.IsConnected) plc.Write("DB5.DBX0.0", 1);
        }

        public static async Task p1_unclenchPenola()
        {
            logger.Info("Разжать пиноли");
            plc.Open();
            if (plc.IsConnected) plc.Write("DB5.DBX0.1", 1);
        }

        //
        public static void p2(bool state)
        {
            logger.Info("Р2 " + state);
            plc.Open();
            if (plc.IsConnected) plc.Write("DB6.DBX0.0", Convert.ToInt32(state));
        }

        public static void p3(bool state)
        {
            logger.Info("Р3 " + state);
            plc.Open();
            if (plc.IsConnected) plc.Write("DB6.DBX0.1", Convert.ToInt32(state));
        }

        public static void p4(bool state)
        {
            logger.Info("Р4 " + state);
            plc.Open();
            if (plc.IsConnected) plc.Write("DB6.DBX0.2", Convert.ToInt32(state));
        }

        public static void p5(bool state)
        {
            logger.Info("Р5 " + state);
            plc.Open();
            if (plc.IsConnected) plc.Write("DB6.DBX0.3", Convert.ToInt32(state));
        }

        private static void all_P(bool state)
        {
            logger.Info("Все Р " + state);
            plc.Open();
            if (plc.IsConnected)
            {
                plc.Write("DB6.DBX0.0", Convert.ToInt32(state)); //p2
                plc.Write("DB6.DBX0.1", Convert.ToInt32(state)); //p3
                plc.Write("DB6.DBX0.2", Convert.ToInt32(state)); //p4
                plc.Write("DB6.DBX0.3", Convert.ToInt32(state)); //p5
            }
        }
    }
}