﻿using System;
using System.IO.Ports;
using System.Windows;
using NLog;

namespace Runout.Utils
{
    public static class ComportWorks
    {
        public static SerialPort serialPort1 = new SerialPort("COM1", 115200);
        public static SerialPort serialPort2 = new SerialPort("COM2", 115200);
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        public static void OpenPorts()
        {
            if (!serialPort1.IsOpen)
                serialPort1.Open();
            if (!serialPort2.IsOpen)
                serialPort2.Open();
        }

        public static void ClosePorts()
        {
            if (serialPort1.IsOpen)
                serialPort1.Close();
            if (serialPort2.IsOpen)
                serialPort2.Close();
        }

        public static void Sensors_On()
        {
            try
            {
                OpenPorts();
                var send = "#00ON" + "\r";
                serialPort1.WriteLine(send);
                serialPort2.WriteLine(send);
                ClosePorts();
            }
            catch (Exception e)
            {
                logger.Error(e);
                throw;
            }
        }

        public static void Sensors_Off()
        {
            try
            {
                OpenPorts();
                var send = "#00OF" + "\r";
                serialPort1.WriteLine(send);
                serialPort2.WriteLine(send);
                ClosePorts();
            }
            catch (Exception e)
            {
                logger.Error(e);
                MessageBox.Show("Error: " + e);
            }
        }
    }
}