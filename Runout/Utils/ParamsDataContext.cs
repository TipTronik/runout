﻿using System.Collections.Generic;
using System.Linq;
using Runout.Dao;
using Runout.Models.Classes;

namespace Runout.Utils
{
    public class ParamsDataContext
    {
        public IList<Wheelset> Wheelsets => Repository<Wheelset>.All().ToList();

        public IList<Operator> Operators => Repository<Operator>.All().ToList();
    }
}