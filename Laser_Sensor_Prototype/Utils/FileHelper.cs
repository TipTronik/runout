﻿using System.Collections.Generic;
using System.Globalization;
using System.IO;

namespace Laser_Sensor_Prototype
{
    internal static class FileHelper
    {
        public static List<double> ReadData(string fileName)
        {
            var counter = 0;
            string line;
            var buffer = new List<double>();
            // Read the file and display it line by line.
            var file = new StreamReader(@fileName);
            while ((line = file.ReadLine()) != null)
            {
                buffer.Add(double.Parse(line, CultureInfo.InvariantCulture));
                counter++;
            }

            file.Close();
            return buffer;
        }
    }
}