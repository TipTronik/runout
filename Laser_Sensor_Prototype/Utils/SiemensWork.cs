﻿using System;
using System.IO.Ports;
using System.Threading;
using S7.Net;

namespace Laser_Sensor_Prototype.Utils
{
    public static class SiemensWork
    {
        private static readonly string deviceIpAddress = "192.168.0.222";
        private static readonly short rackNumber = 0;
        private static readonly short slotNumber = 1;
        private static readonly Plc plc = new Plc(CpuType.S71200, deviceIpAddress, rackNumber, slotNumber);

        public static void liftersUp()
        {
            left_lifter_up();
            right_lifter_up();
        }

        public static bool toStartPosition()
        {
            kp_stop();
            Thread.Sleep(3000);
            p1_unclenchPenola();
            all_P(false);
            left_lifter_null();
            right_lifter_null();

            return true;
        }

        public static void preliminaryLift()
        {
            left_lifter_up();
            right_lifter_up();
            Thread.Sleep(10000);
            left_lifter_stop();
            right_lifter_stop();
        }

        public static void step_3()
        {
            var measureSensor_Right = new Sensor(new Channel(new SerialPort("COM2", 115200), 1), 2);
            var measureSensor_Left = new Sensor(new Channel(new SerialPort("COM1", 115200), 1), 11);

            measureSensor_Right.channel.serialPort.Open();
            measureSensor_Left.channel.serialPort.Open();

            p2(true);
            p3(true);
            p4(true);
            p5(true);

            left_lifter_up();
            right_lifter_up();
            var lift = true;
            var lift_right = true;
            var lift_left = true;
            while (lift)
            {
                if (measureSensor_Right.ReciveData() > 0.86 && measureSensor_Right.ReciveData() < 1)
                {
                    lift_right = false;

                    right_lifter_stop();
                }
                if (measureSensor_Left.ReciveData() > 0.86 && measureSensor_Left.ReciveData() < 1)
                {
                    lift_left = false;

                    left_lifter_stop();
                }

                if (lift_left == false && lift_right == false)
                {
                    lift = false;
                }
            }

            lifters_stop();
            measureSensor_Right.channel.serialPort.Close();
            measureSensor_Left.channel.serialPort.Close();
        }

        public static void step_4()
        {
            p1_pinchPenola();
            Thread.Sleep(10000);
            kp_start();
            //
        }

        public static void step_5()
        {
            //выкл датчики
            kp_stop();
            all_P(false);
            p1_unclenchPenola();
            Thread.Sleep(10000);
            lifters_null();
        }

        public static void kp_start()
        {
            plc.Open();
            if (plc.IsConnected)
            {
                var val = 900;
                plc.Write("DB3.DBW2", val);
                var d = Convert.ToDouble(plc.Read("DB3.DBD2"));
                plc.Write("DB3.DBX0.0", 1);
                //Get data
            }
        }

        public static void kp_stop()
        {
            plc.Open();
            if (plc.IsConnected)
            {
                plc.Write("DB3.DBX0.2", 1);
                //Get data
            }
        }

        public static void lifters_stop()
        {
            left_lifter_stop();
            right_lifter_stop();
        }

        public static void lifters_null()
        {
            left_lifter_null();
            right_lifter_null();
        }

        private static void left_lifter_up()
        {
            plc.Open();
            if (plc.IsConnected)
            {
                plc.Write("DB2.DBX0.0", 1);
                //Get data
            }
        }

        private static void right_lifter_up()
        {
            plc.Open();
            if (plc.IsConnected)
            {
                plc.Write("DB4.DBX0.0", 1);
                //Get data
            }
        }

        private static void left_lifter_stop()
        {
            plc.Open();
            if (plc.IsConnected)
            {
                plc.Write("DB2.DBX0.2", 1);
                //Get data
            }
        }

        private static void right_lifter_stop()
        {
            plc.Open();
            if (plc.IsConnected)
            {
                plc.Write("DB4.DBX0.2", 1);
                //Get data
            }
        }

        private static void left_lifter_null()
        {
            plc.Open();
            if (plc.IsConnected)
            {
                plc.Write("DB2.DBX0.3", 1);
                //Get data
            }
        }

        private static void right_lifter_null()
        {
            plc.Open();
            if (plc.IsConnected)
            {
                plc.Write("DB4.DBX0.3", 1);
                //Get data
            }
        }

        public static void p1_pinchPenola()
        {
            plc.Open();
            if (plc.IsConnected)
            {
                plc.Write("DB5.DBX0.0", 1);
                //Get data
            }
        }

        public static void p1_unclenchPenola()
        {
            plc.Open();
            if (plc.IsConnected)
            {
                plc.Write("DB5.DBX0.1", 1);
                //Get data
            }
        }

        public static void p2(bool state)
        {
            plc.Open();
            if (plc.IsConnected)
            {
                plc.Write("DB6.DBX0.0", Convert.ToInt32(state));
                //Get data
            }
        }

        public static void p3(bool state)
        {
            plc.Open();
            if (plc.IsConnected)
            {
                plc.Write("DB6.DBX0.1", Convert.ToInt32(state));
                //Get data
            }
        }

        public static void p4(bool state)
        {
            plc.Open();
            if (plc.IsConnected)
            {
                plc.Write("DB6.DBX0.2", Convert.ToInt32(state));
                //Get data
            }
        }

        public static void p5(bool state)
        {
            plc.Open();
            if (plc.IsConnected)
            {
                plc.Write("DB6.DBX0.3", Convert.ToInt32(state));
                //Get data
            }
        }

        private static void all_P(bool state)
        {
            plc.Open();
            if (plc.IsConnected)
            {
                plc.Write("DB6.DBX0.0", Convert.ToInt32(state)); //p2
                plc.Write("DB6.DBX0.1", Convert.ToInt32(state)); //p3
                plc.Write("DB6.DBX0.2", Convert.ToInt32(state)); //p4
                plc.Write("DB6.DBX0.3", Convert.ToInt32(state)); //p5
            }
        }
    }

    public static class SiemensWork_debug
    {
        public static bool startDebug()
        {
            plc.Open();
            if (plc.IsConnected)
            {
                plc.Write("DB1.DBX18.4", 1);
                return (bool) plc.Read("DB1.DBX18.4");
            }
            return false;
        }

        public static void pinchPenola_debug()
        {
            plc.Open();
            if (plc.IsConnected)
            {
                plc.Write("DB1.DBX0.0", 1);
                //Get data
            }
        }

        public static void unclenchPenola_Debug()
        {
            plc.Open();
            if (plc.IsConnected)
            {
                plc.Write("DB1.DBX0.0", 0);
                //Get data
            }
        }

        public static void p2(bool state)
        {
            plc.Open();
            if (plc.IsConnected)
            {
                plc.Write("DB1.DBX0.1", Convert.ToInt32(state));
                //Get data
            }
        }

        public static void p3(bool state)
        {
            plc.Open();
            if (plc.IsConnected)
            {
                plc.Write("DB1.DBX0.2", Convert.ToInt32(state));
                //Get data
            }
        }

        public static void p4(bool state)
        {
            plc.Open();
            if (plc.IsConnected)
            {
                plc.Write("DB1.DBX0.3", Convert.ToInt32(state));
                //Get data
            }
        }

        public static void p5(bool state)
        {
            plc.Open();
            if (plc.IsConnected)
            {
                plc.Write("DB1.DBX0.4", Convert.ToInt32(state));
                //Get data
            }
        }

        public static void direction_LeftLifter(int i)
        {
            plc.Open();
            if (plc.IsConnected)
            {
                plc.Write("DB1.DBX6.0", i);
                //Get data
            }
        }

        public static void direction_RightLifter(int i)
        {
            plc.Open();
            if (plc.IsConnected)
            {
                plc.Write("DB1.DBX12.0", i);
                //Get data
            }
        }

        public static void switch_LeftLifter(bool state)
        {
            plc.Open();
            if (plc.IsConnected)
            {
                plc.Write("DB1.DBX6.1", Convert.ToInt32(state));
                //Get data
            }
        }

        public static void switch_RightLifter(bool state)
        {
            plc.Open();
            if (plc.IsConnected)
            {
                plc.Write("DB1.DBX12.1", Convert.ToInt32(state));
                //Get data
            }
        }

        public static void setup_LeftLifter(double value)
        {
            plc.Open();
            if (plc.IsConnected)
            {
                plc.Write("DB1.DBD2", value);
            }
        }

        public static void setup_RightLifter(double value)
        {
            plc.Open();
            if (plc.IsConnected)
            {
                plc.Write("DB1.DBD8", value);
            }
        }

        public static void setupKP(double value)
        {
            plc.Open();
            if (plc.IsConnected)
            {
                plc.Write("DB1.DBD14", value);
            }
        }

        public static void direction_KP(int i)
        {
            plc.Open();
            if (plc.IsConnected)
            {
                plc.Write("DB1.DBX18.0", i);
                //Get data
            }
        }

        public static void switchKP(bool state)
        {
            plc.Open();
            if (plc.IsConnected)
            {
                plc.Write("DB1.DBX18.1", state);
                //Get data
            }
        }

        public static void start()
        {
            plc.Open();
            if (plc.IsConnected)
            {
                plc.Write("DB1.DBX18.4", true);
                //Get data
            }
        }

        public static bool switcherLeftLifter()
        {
            plc.Open();
            if (plc.IsConnected)
            {
                //Get data
                return (bool) plc.Read("DB1.DBX18.2");
            }
            return false;
        }

        public static bool switcherRightLifter()
        {
            plc.Open();
            if (plc.IsConnected)
            {
                //Get data
                return (bool) plc.Read("DB1.DBX18.3");
            }
            return false;
        }

        private static readonly string deviceIpAddress = "192.168.0.222";
        private static readonly short rackNumber = 0;
        private static readonly short slotNumber = 1;
        private static readonly Plc plc = new Plc(CpuType.S71200, deviceIpAddress, rackNumber, slotNumber);
    }
}