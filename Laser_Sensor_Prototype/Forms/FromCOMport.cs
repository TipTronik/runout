﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Ports;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using Laser_Sensor_Prototype.Forms;


namespace Laser_Sensor_Prototype
{
    public partial class FromCOMport : Form
    {
        private List<double> bufferList = new List<double>();
        private Thread t;
        private readonly List<Sensor> sensors = new List<Sensor>();

        public FromCOMport()
        {
            InitializeComponent();

            var chan1 = new Channel(serialPort2, 1);
            var chan2 = new Channel(serialPort1, 2);

            for (var i = 1; i <= 12; ++i)
            {
                if (i <= 4)
                {
                    sensors.Add(new Sensor(chan1, i));
                }
                if (i > 4)
                {
                    sensors.Add(new Sensor(chan2, i));
                }
                dataGridView1.Rows.Add(i, "");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            serialPort1.Open();
            serialPort2.Open();

            var send = "#00ON" + "\r";
            serialPort1.WriteLine(send);
            serialPort1.Close();
            serialPort2.WriteLine(send);
            serialPort2.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            serialPort1.Open();
            serialPort2.Open();

            var send = "#00OF" + "\r";
            serialPort1.WriteLine(send);
            serialPort1.Close();
            serialPort2.WriteLine(send);
            serialPort2.Close();
        }

        private void button_start_Click(object sender, EventArgs e)
        {
            serialPort1.Open();
            serialPort2.Open();
            label_mesNum.Text = "0";
            t = new Thread(readData);
            t.Start();
        }

        private void button_stop_Click(object sender, EventArgs e)
        {
            t.Abort();

            serialPort1.Close();
            serialPort2.Close();
        }

        private double MedianFilter(List<double> buffer)
        {
            var window = 9;
            if (buffer.Count == window)
            {
                var sortBuffer = new List<double>(buffer);
                sortBuffer.Sort();
                return sortBuffer[(window/2) + 1];
            }
            if (buffer.Count > window)
            {
                buffer.RemoveAt(0);
                var sortBuffer = new List<double>(buffer);
                sortBuffer.Sort();
                return sortBuffer[(window/2) + 1];
            }
            if (buffer.Count < window)
            {
                var sortBuffer = new List<double>(buffer);
                sortBuffer.Sort();
                return sortBuffer[(sortBuffer.Count/2)];
            }
            return 0;
        }

        private double Beating(List<double> data)
        {
            data.Sort();
            return data[data.Count - 1] - data[0];
        }

        private void readData()
        {
            var allData = new List<double>();

            int i = 0;
            while (true)
            {
                var send = "#00FX" + "\r";
                serialPort1.Write(send);
                serialPort2.Write(send);
                Thread.Sleep(5);

                foreach (var sensor in sensors)
                {
                    var reciveData = sensor.ReciveData();
                    dataGridView1.BeginInvoke(
                        new Action<string>(s => dataGridView1.Rows[sensor.address - 1].Cells[1].Value = s),
                        reciveData.ToString());

                    sensor.listData.Add(reciveData);
                    dataGridView1.BeginInvoke(
                        new Action<string>(s => dataGridView1.Rows[sensor.address - 1].Cells[2].Value = s),
                        Beating(sensor.listData).ToString());
                }
                ++i;
                label_mesNum.BeginInvoke(new Action<string>(s => label_mesNum.Text = s), i.ToString());
            }
        }

        private void button_upLift_Click(object sender, EventArgs e)
        {
            var df = new DebugForm();
            df.Show();
        }

        private void button_saveData_Click(object sender, EventArgs e)
        {
            try
            {
                StreamWriter savefile;  //Класс для записи в файл
                System.IO.Directory.CreateDirectory("./Data/");
                string fName = DateTime.Now.ToString("hhmmssddMMyyyy");
                savefile = new StreamWriter("./Data/" + fName + ".txt", false, Encoding.Unicode);
                foreach (var sensor in sensors)
                {
                    savefile.WriteLine("Sensor " + sensor.address +"; Value: " + dataGridView1.Rows[sensor.address - 1].Cells[1].Value + "; Beating: " + dataGridView1.Rows[sensor.address - 1].Cells[2].Value);
                }
                savefile.Close(); // Закрываем файл
                MessageBox.Show("Отчет сформирован в файл: " + fName);
            }
            catch
            {
                throw;
            }
        }
    }
}