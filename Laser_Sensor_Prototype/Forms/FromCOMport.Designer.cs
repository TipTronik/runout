﻿namespace Laser_Sensor_Prototype
{
    partial class FromCOMport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.serialPort1 = new System.IO.Ports.SerialPort(this.components);
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button_start = new System.Windows.Forms.Button();
            this.button_stop = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.address = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.data = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.beat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.serialPort2 = new System.IO.Ports.SerialPort(this.components);
            this.button_upLift = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label_mesNum = new System.Windows.Forms.Label();
            this.button_saveData = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // serialPort1
            // 
            this.serialPort1.BaudRate = 115200;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(142, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Включить";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(12, 41);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(142, 23);
            this.button2.TabIndex = 1;
            this.button2.Text = "Выключить";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button_start
            // 
            this.button_start.Location = new System.Drawing.Point(160, 12);
            this.button_start.Name = "button_start";
            this.button_start.Size = new System.Drawing.Size(142, 23);
            this.button_start.TabIndex = 2;
            this.button_start.Text = "Начать опрос";
            this.button_start.UseVisualStyleBackColor = true;
            this.button_start.Click += new System.EventHandler(this.button_start_Click);
            // 
            // button_stop
            // 
            this.button_stop.Location = new System.Drawing.Point(160, 41);
            this.button_stop.Name = "button_stop";
            this.button_stop.Size = new System.Drawing.Size(142, 23);
            this.button_stop.TabIndex = 3;
            this.button_stop.Text = "Стоп";
            this.button_stop.UseVisualStyleBackColor = true;
            this.button_stop.Click += new System.EventHandler(this.button_stop_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.address,
            this.data,
            this.beat});
            this.dataGridView1.Location = new System.Drawing.Point(12, 94);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(446, 332);
            this.dataGridView1.TabIndex = 6;
            // 
            // address
            // 
            this.address.HeaderText = "Адрес";
            this.address.Name = "address";
            this.address.ReadOnly = true;
            // 
            // data
            // 
            this.data.HeaderText = "Значение";
            this.data.Name = "data";
            this.data.ReadOnly = true;
            // 
            // beat
            // 
            this.beat.HeaderText = "Биение";
            this.beat.Name = "beat";
            this.beat.ReadOnly = true;
            // 
            // serialPort2
            // 
            this.serialPort2.BaudRate = 115200;
            this.serialPort2.PortName = "COM2";
            // 
            // button_upLift
            // 
            this.button_upLift.Location = new System.Drawing.Point(308, 12);
            this.button_upLift.Name = "button_upLift";
            this.button_upLift.Size = new System.Drawing.Size(75, 52);
            this.button_upLift.TabIndex = 7;
            this.button_upLift.Text = "Отладочное окно";
            this.button_upLift.UseVisualStyleBackColor = true;
            this.button_upLift.Click += new System.EventHandler(this.button_upLift_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(12, 67);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(189, 24);
            this.label1.TabIndex = 8;
            this.label1.Text = "Число измерений:";
            // 
            // label_mesNum
            // 
            this.label_mesNum.AutoSize = true;
            this.label_mesNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_mesNum.Location = new System.Drawing.Point(207, 67);
            this.label_mesNum.Name = "label_mesNum";
            this.label_mesNum.Size = new System.Drawing.Size(21, 24);
            this.label_mesNum.TabIndex = 9;
            this.label_mesNum.Text = "0";
            // 
            // button_saveData
            // 
            this.button_saveData.Location = new System.Drawing.Point(389, 12);
            this.button_saveData.Name = "button_saveData";
            this.button_saveData.Size = new System.Drawing.Size(69, 52);
            this.button_saveData.TabIndex = 10;
            this.button_saveData.Text = "Сохранить данные";
            this.button_saveData.UseVisualStyleBackColor = true;
            this.button_saveData.Click += new System.EventHandler(this.button_saveData_Click);
            // 
            // FromCOMport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(470, 438);
            this.Controls.Add(this.button_saveData);
            this.Controls.Add(this.label_mesNum);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button_upLift);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.button_stop);
            this.Controls.Add(this.button_start);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Name = "FromCOMport";
            this.Text = "FromCOMport";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.IO.Ports.SerialPort serialPort1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button_start;
        private System.Windows.Forms.Button button_stop;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn address;
        private System.Windows.Forms.DataGridViewTextBoxColumn data;
        private System.IO.Ports.SerialPort serialPort2;
        private System.Windows.Forms.DataGridViewTextBoxColumn beat;
        private System.Windows.Forms.Button button_upLift;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label_mesNum;
        private System.Windows.Forms.Button button_saveData;
    }
}