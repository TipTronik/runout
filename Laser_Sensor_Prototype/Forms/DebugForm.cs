﻿using System;
using System.Windows.Forms;
using Laser_Sensor_Prototype.Utils;

namespace Laser_Sensor_Prototype.Forms
{
    public partial class DebugForm : Form
    {
        public DebugForm()
        {
            InitializeComponent();
            //if (!Utils.SiemensWork_debug.startDebug())
            //{
            //    MessageBox.Show("Ошибка!");
            //}
        }

        private void button_pinchPenola_Click(object sender, EventArgs e)
        {
            SiemensWork.p1_pinchPenola();
        }

        private void button_unclenchPenola_Click(object sender, EventArgs e)
        {
            SiemensWork.p1_unclenchPenola();
        }

        private void button_r2_down_Click(object sender, EventArgs e)
        {
            //Utils.SiemensWork_debug.p2(false);
            SiemensWork.p2(false);
        }

        private void button_r2_up_Click(object sender, EventArgs e)
        {
            SiemensWork.p2(true);
        }

        private void button_p3_down_Click(object sender, EventArgs e)
        {
            SiemensWork.p3(false);
        }

        private void button_r3_up_Click(object sender, EventArgs e)
        {
            SiemensWork.p3(true);
        }

        private void button_p4_down_Click(object sender, EventArgs e)
        {
            SiemensWork.p4(false);
        }

        private void button_p4_up_Click(object sender, EventArgs e)
        {
            SiemensWork.p4(true);
        }

        private void button_p5_down_Click(object sender, EventArgs e)
        {
            SiemensWork.p5(false);
        }

        private void button_p5_up_Click(object sender, EventArgs e)
        {
            SiemensWork.p5(true);
        }

        private void button_dirLL_Click(object sender, EventArgs e)
        {
            SiemensWork_debug.direction_LeftLifter(0);
        }

        private void button_dirLL_1_Click(object sender, EventArgs e)
        {
            SiemensWork_debug.direction_LeftLifter(1);
        }

        private void button_LL_On_Click(object sender, EventArgs e)
        {
            SiemensWork_debug.switch_LeftLifter(true);
        }

        private void button_LL_Off_Click(object sender, EventArgs e)
        {
            SiemensWork_debug.switch_LeftLifter(false);
        }

        private void button_leftLifterSetup_Click(object sender, EventArgs e)
        {
            SiemensWork_debug.setup_LeftLifter(double.Parse(textBox_leftLifter.Text));
        }

        private void button_rightLifterSetup_Click(object sender, EventArgs e)
        {
            SiemensWork_debug.setup_RightLifter(double.Parse(textBox_rightLifter.Text));
        }

        private void button_dirRL_0_Click(object sender, EventArgs e)
        {
            SiemensWork_debug.direction_RightLifter(0);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            SiemensWork_debug.direction_RightLifter(1);
        }

        private void button_RL_On_Click(object sender, EventArgs e)
        {
            SiemensWork_debug.switch_RightLifter(true);
        }

        private void button_RL_Off_Click(object sender, EventArgs e)
        {
            SiemensWork_debug.switch_RightLifter(false);
        }

        private void button_setupKP_Click(object sender, EventArgs e)
        {
            SiemensWork_debug.setupKP(double.Parse(textBox_setup_KP.Text));
        }

        private void button_kpDir_0_Click(object sender, EventArgs e)
        {
            SiemensWork_debug.direction_KP(0);
        }

        private void button_dirKP_1_Click(object sender, EventArgs e)
        {
            SiemensWork_debug.direction_KP(1);
        }

        private void button_KP_On_Click(object sender, EventArgs e)
        {
            //SiemensWork_debug.switchKP(true);
            SiemensWork.kp_start();
        }

        private void button_KP_Off_Click(object sender, EventArgs e)
        {
            //SiemensWork_debug.switchKP(false);
            SiemensWork.kp_stop();
        }

        private void button_knplp_0_Click(object sender, EventArgs e)
        {
            label3.Text = SiemensWork_debug.switcherLeftLifter().ToString();
        }

        private void button18_Click(object sender, EventArgs e)
        {
            SiemensWork_debug.start();
        }

        private void button_knppp_req_Click(object sender, EventArgs e)
        {
            label4.Text = SiemensWork_debug.switcherRightLifter().ToString();
        }

        private void button_step1_Click(object sender, EventArgs e)
        {
            SiemensWork.toStartPosition();
        }

        private void button_step2_Click(object sender, EventArgs e)
        {
            SiemensWork.preliminaryLift();
        }

        private void button_step3_Click(object sender, EventArgs e)
        {
            SiemensWork.step_3();
        }

        private void button_step4_Click(object sender, EventArgs e)
        {
            SiemensWork.step_4();
        }

        private void button_step5_Click(object sender, EventArgs e)
        {
            SiemensWork.step_5();
        }

        private void button_stopLift_Click(object sender, EventArgs e)
        {
            SiemensWork.lifters_stop();
        }

        private void button_liftNull_Click(object sender, EventArgs e)
        {
            SiemensWork.lifters_null();
        }

        private void button_upAll_Click(object sender, EventArgs e)
        {
            SiemensWork.liftersUp();
        }
    }
}