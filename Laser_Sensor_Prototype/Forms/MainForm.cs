﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Laser_Sensor_Prototype
{
    public partial class MainForm : Form
    {
        public List<double> DataList = new List<double>();

        public MainForm()
        {
            InitializeComponent();
        }

        private void OpenFile_ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var openFileDialog = new OpenFileDialog();
            openFileDialog.ShowDialog();
            if (openFileDialog.FileName.Length > 0)
            {
                DataList = FileHelper.ReadData(openFileDialog.FileName);
                DrawGraph();
            }
        }

        private void DrawGraph()
        {
            chart_raw.Series[0].Points.Clear();

            for (var i = 0; i < DataList.Count; i++)
            {
                chart_raw.Series[0].Points.Add(DataList[i]);
            }

            chart_signal.Series[0].Points.Clear();
            var peaks = Analyzer.FindPeak(DataList).ToArray();

            for (var i = 0; i < peaks.Length; i++)
            {
                chart_signal.Series[0].Points.Add(peaks[i]);
            }

            var average = Analyzer.MovingAverage(peaks, int.Parse(textBox1.Text));
            chart_signal.Series[1].Points.Clear();

            for (var i = 0; i < average.Length; i++)
            {
                chart_signal.Series[1].Points.Add(average[i]);
            }
            label_beating.Text = Analyzer.Beating(average.ToList()).ToString();
        }

        private void другоеОкноToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }
    }
}