﻿namespace Laser_Sensor_Prototype.Forms
{
    partial class DebugForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DebugForm));
            this.panel1 = new System.Windows.Forms.Panel();
            this.button_unclenchPenola = new System.Windows.Forms.Button();
            this.button_pinchPenola = new System.Windows.Forms.Button();
            this.groupBox_SwitchLeftLifter = new System.Windows.Forms.GroupBox();
            this.button_LL_Off = new System.Windows.Forms.Button();
            this.button_LL_On = new System.Windows.Forms.Button();
            this.groupBox_directionLeftLifter = new System.Windows.Forms.GroupBox();
            this.button_dirLL_1 = new System.Windows.Forms.Button();
            this.button_dirLL_0 = new System.Windows.Forms.Button();
            this.button_r2_down = new System.Windows.Forms.Button();
            this.button_r2_up = new System.Windows.Forms.Button();
            this.gb_r2 = new System.Windows.Forms.GroupBox();
            this.groupBox_P3 = new System.Windows.Forms.GroupBox();
            this.button_r3_up = new System.Windows.Forms.Button();
            this.button_p3_down = new System.Windows.Forms.Button();
            this.groupBox_p4 = new System.Windows.Forms.GroupBox();
            this.button_p4_up = new System.Windows.Forms.Button();
            this.button_p4_down = new System.Windows.Forms.Button();
            this.groupBox_p5 = new System.Windows.Forms.GroupBox();
            this.button_p5_up = new System.Windows.Forms.Button();
            this.button_p5_down = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox_leftLifter = new System.Windows.Forms.TextBox();
            this.button_leftLifterSetup = new System.Windows.Forms.Button();
            this.button_rightLifterSetup = new System.Windows.Forms.Button();
            this.textBox_rightLifter = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button_RL_Off = new System.Windows.Forms.Button();
            this.button_RL_On = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button_dirRL_1 = new System.Windows.Forms.Button();
            this.button_dirRL_0 = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.button_setupKP = new System.Windows.Forms.Button();
            this.textBox_setup_KP = new System.Windows.Forms.TextBox();
            this.groupBox_kp_Direct = new System.Windows.Forms.GroupBox();
            this.button_dirKP_1 = new System.Windows.Forms.Button();
            this.button_kpDir_0 = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.button_KP_Off = new System.Windows.Forms.Button();
            this.button_KP_On = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.button_knplp_req = new System.Windows.Forms.Button();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.button_knppp_req = new System.Windows.Forms.Button();
            this.button18 = new System.Windows.Forms.Button();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.button_liftNull = new System.Windows.Forms.Button();
            this.button_stopRotation = new System.Windows.Forms.Button();
            this.button_stopLift = new System.Windows.Forms.Button();
            this.button_step5 = new System.Windows.Forms.Button();
            this.button_step4 = new System.Windows.Forms.Button();
            this.button_step3 = new System.Windows.Forms.Button();
            this.button_step2 = new System.Windows.Forms.Button();
            this.button_step1 = new System.Windows.Forms.Button();
            this.button_upAll = new System.Windows.Forms.Button();
            this.label_sensVal = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.groupBox_SwitchLeftLifter.SuspendLayout();
            this.groupBox_directionLeftLifter.SuspendLayout();
            this.gb_r2.SuspendLayout();
            this.groupBox_P3.SuspendLayout();
            this.groupBox_p4.SuspendLayout();
            this.groupBox_p5.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox_kp_Direct.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel1.BackgroundImage")));
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel1.Controls.Add(this.button_unclenchPenola);
            this.panel1.Controls.Add(this.button_pinchPenola);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(984, 617);
            this.panel1.TabIndex = 0;
            // 
            // button_unclenchPenola
            // 
            this.button_unclenchPenola.Location = new System.Drawing.Point(824, 314);
            this.button_unclenchPenola.Name = "button_unclenchPenola";
            this.button_unclenchPenola.Size = new System.Drawing.Size(104, 23);
            this.button_unclenchPenola.TabIndex = 1;
            this.button_unclenchPenola.Text = "Разжать пеноли";
            this.button_unclenchPenola.UseVisualStyleBackColor = true;
            this.button_unclenchPenola.Click += new System.EventHandler(this.button_unclenchPenola_Click);
            // 
            // button_pinchPenola
            // 
            this.button_pinchPenola.Location = new System.Drawing.Point(824, 285);
            this.button_pinchPenola.Name = "button_pinchPenola";
            this.button_pinchPenola.Size = new System.Drawing.Size(104, 23);
            this.button_pinchPenola.TabIndex = 0;
            this.button_pinchPenola.Text = "Зажать пеноли";
            this.button_pinchPenola.UseVisualStyleBackColor = true;
            this.button_pinchPenola.Click += new System.EventHandler(this.button_pinchPenola_Click);
            // 
            // groupBox_SwitchLeftLifter
            // 
            this.groupBox_SwitchLeftLifter.Controls.Add(this.button_LL_Off);
            this.groupBox_SwitchLeftLifter.Controls.Add(this.button_LL_On);
            this.groupBox_SwitchLeftLifter.Location = new System.Drawing.Point(1002, 442);
            this.groupBox_SwitchLeftLifter.Name = "groupBox_SwitchLeftLifter";
            this.groupBox_SwitchLeftLifter.Size = new System.Drawing.Size(137, 80);
            this.groupBox_SwitchLeftLifter.TabIndex = 15;
            this.groupBox_SwitchLeftLifter.TabStop = false;
            this.groupBox_SwitchLeftLifter.Text = "Включение ЛП";
            // 
            // button_LL_Off
            // 
            this.button_LL_Off.Location = new System.Drawing.Point(6, 48);
            this.button_LL_Off.Name = "button_LL_Off";
            this.button_LL_Off.Size = new System.Drawing.Size(125, 23);
            this.button_LL_Off.TabIndex = 1;
            this.button_LL_Off.Text = "Выкл";
            this.button_LL_Off.UseVisualStyleBackColor = true;
            this.button_LL_Off.Click += new System.EventHandler(this.button_LL_Off_Click);
            // 
            // button_LL_On
            // 
            this.button_LL_On.Location = new System.Drawing.Point(6, 19);
            this.button_LL_On.Name = "button_LL_On";
            this.button_LL_On.Size = new System.Drawing.Size(125, 23);
            this.button_LL_On.TabIndex = 0;
            this.button_LL_On.Text = "Вкл";
            this.button_LL_On.UseVisualStyleBackColor = true;
            this.button_LL_On.Click += new System.EventHandler(this.button_LL_On_Click);
            // 
            // groupBox_directionLeftLifter
            // 
            this.groupBox_directionLeftLifter.Controls.Add(this.button_dirLL_1);
            this.groupBox_directionLeftLifter.Controls.Add(this.button_dirLL_0);
            this.groupBox_directionLeftLifter.Location = new System.Drawing.Point(1002, 356);
            this.groupBox_directionLeftLifter.Name = "groupBox_directionLeftLifter";
            this.groupBox_directionLeftLifter.Size = new System.Drawing.Size(137, 80);
            this.groupBox_directionLeftLifter.TabIndex = 14;
            this.groupBox_directionLeftLifter.TabStop = false;
            this.groupBox_directionLeftLifter.Text = "Направление ЛП";
            // 
            // button_dirLL_1
            // 
            this.button_dirLL_1.Location = new System.Drawing.Point(6, 48);
            this.button_dirLL_1.Name = "button_dirLL_1";
            this.button_dirLL_1.Size = new System.Drawing.Size(125, 23);
            this.button_dirLL_1.TabIndex = 1;
            this.button_dirLL_1.Text = "1";
            this.button_dirLL_1.UseVisualStyleBackColor = true;
            this.button_dirLL_1.Click += new System.EventHandler(this.button_dirLL_1_Click);
            // 
            // button_dirLL_0
            // 
            this.button_dirLL_0.Location = new System.Drawing.Point(6, 19);
            this.button_dirLL_0.Name = "button_dirLL_0";
            this.button_dirLL_0.Size = new System.Drawing.Size(125, 23);
            this.button_dirLL_0.TabIndex = 0;
            this.button_dirLL_0.Text = "0";
            this.button_dirLL_0.UseVisualStyleBackColor = true;
            this.button_dirLL_0.Click += new System.EventHandler(this.button_dirLL_Click);
            // 
            // button_r2_down
            // 
            this.button_r2_down.Location = new System.Drawing.Point(12, 19);
            this.button_r2_down.Name = "button_r2_down";
            this.button_r2_down.Size = new System.Drawing.Size(119, 23);
            this.button_r2_down.TabIndex = 1;
            this.button_r2_down.Text = "Р2 Опустить";
            this.button_r2_down.UseVisualStyleBackColor = true;
            this.button_r2_down.Click += new System.EventHandler(this.button_r2_down_Click);
            // 
            // button_r2_up
            // 
            this.button_r2_up.Location = new System.Drawing.Point(12, 48);
            this.button_r2_up.Name = "button_r2_up";
            this.button_r2_up.Size = new System.Drawing.Size(119, 23);
            this.button_r2_up.TabIndex = 2;
            this.button_r2_up.Text = "Р2 Поднять";
            this.button_r2_up.UseVisualStyleBackColor = true;
            this.button_r2_up.Click += new System.EventHandler(this.button_r2_up_Click);
            // 
            // gb_r2
            // 
            this.gb_r2.Controls.Add(this.button_r2_up);
            this.gb_r2.Controls.Add(this.button_r2_down);
            this.gb_r2.Location = new System.Drawing.Point(1002, 12);
            this.gb_r2.Name = "gb_r2";
            this.gb_r2.Size = new System.Drawing.Size(137, 80);
            this.gb_r2.TabIndex = 4;
            this.gb_r2.TabStop = false;
            this.gb_r2.Text = "P2";
            // 
            // groupBox_P3
            // 
            this.groupBox_P3.Controls.Add(this.button_r3_up);
            this.groupBox_P3.Controls.Add(this.button_p3_down);
            this.groupBox_P3.Location = new System.Drawing.Point(1002, 98);
            this.groupBox_P3.Name = "groupBox_P3";
            this.groupBox_P3.Size = new System.Drawing.Size(137, 80);
            this.groupBox_P3.TabIndex = 5;
            this.groupBox_P3.TabStop = false;
            this.groupBox_P3.Text = "P3";
            // 
            // button_r3_up
            // 
            this.button_r3_up.Location = new System.Drawing.Point(6, 48);
            this.button_r3_up.Name = "button_r3_up";
            this.button_r3_up.Size = new System.Drawing.Size(125, 23);
            this.button_r3_up.TabIndex = 2;
            this.button_r3_up.Text = "Р3 Поднять";
            this.button_r3_up.UseVisualStyleBackColor = true;
            this.button_r3_up.Click += new System.EventHandler(this.button_r3_up_Click);
            // 
            // button_p3_down
            // 
            this.button_p3_down.Location = new System.Drawing.Point(6, 19);
            this.button_p3_down.Name = "button_p3_down";
            this.button_p3_down.Size = new System.Drawing.Size(125, 23);
            this.button_p3_down.TabIndex = 1;
            this.button_p3_down.Text = "Р3 Опустить";
            this.button_p3_down.UseVisualStyleBackColor = true;
            this.button_p3_down.Click += new System.EventHandler(this.button_p3_down_Click);
            // 
            // groupBox_p4
            // 
            this.groupBox_p4.Controls.Add(this.button_p4_up);
            this.groupBox_p4.Controls.Add(this.button_p4_down);
            this.groupBox_p4.Location = new System.Drawing.Point(1002, 184);
            this.groupBox_p4.Name = "groupBox_p4";
            this.groupBox_p4.Size = new System.Drawing.Size(137, 80);
            this.groupBox_p4.TabIndex = 6;
            this.groupBox_p4.TabStop = false;
            this.groupBox_p4.Text = "P4";
            // 
            // button_p4_up
            // 
            this.button_p4_up.Location = new System.Drawing.Point(6, 48);
            this.button_p4_up.Name = "button_p4_up";
            this.button_p4_up.Size = new System.Drawing.Size(125, 23);
            this.button_p4_up.TabIndex = 2;
            this.button_p4_up.Text = "Р4 Поднять";
            this.button_p4_up.UseVisualStyleBackColor = true;
            this.button_p4_up.Click += new System.EventHandler(this.button_p4_up_Click);
            // 
            // button_p4_down
            // 
            this.button_p4_down.Location = new System.Drawing.Point(6, 19);
            this.button_p4_down.Name = "button_p4_down";
            this.button_p4_down.Size = new System.Drawing.Size(125, 23);
            this.button_p4_down.TabIndex = 1;
            this.button_p4_down.Text = "Р4 Опустить";
            this.button_p4_down.UseVisualStyleBackColor = true;
            this.button_p4_down.Click += new System.EventHandler(this.button_p4_down_Click);
            // 
            // groupBox_p5
            // 
            this.groupBox_p5.Controls.Add(this.button_p5_up);
            this.groupBox_p5.Controls.Add(this.button_p5_down);
            this.groupBox_p5.Location = new System.Drawing.Point(1002, 270);
            this.groupBox_p5.Name = "groupBox_p5";
            this.groupBox_p5.Size = new System.Drawing.Size(137, 80);
            this.groupBox_p5.TabIndex = 7;
            this.groupBox_p5.TabStop = false;
            this.groupBox_p5.Text = "P5";
            // 
            // button_p5_up
            // 
            this.button_p5_up.Location = new System.Drawing.Point(6, 48);
            this.button_p5_up.Name = "button_p5_up";
            this.button_p5_up.Size = new System.Drawing.Size(125, 23);
            this.button_p5_up.TabIndex = 2;
            this.button_p5_up.Text = "Р5 Поднять";
            this.button_p5_up.UseVisualStyleBackColor = true;
            this.button_p5_up.Click += new System.EventHandler(this.button_p5_up_Click);
            // 
            // button_p5_down
            // 
            this.button_p5_down.Location = new System.Drawing.Point(6, 19);
            this.button_p5_down.Name = "button_p5_down";
            this.button_p5_down.Size = new System.Drawing.Size(125, 23);
            this.button_p5_down.TabIndex = 1;
            this.button_p5_down.Text = "Р5 Опустить";
            this.button_p5_down.UseVisualStyleBackColor = true;
            this.button_p5_down.Click += new System.EventHandler(this.button_p5_down_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 638);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(169, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Установка левого подъемника:";
            // 
            // textBox_leftLifter
            // 
            this.textBox_leftLifter.Location = new System.Drawing.Point(187, 635);
            this.textBox_leftLifter.Name = "textBox_leftLifter";
            this.textBox_leftLifter.Size = new System.Drawing.Size(100, 20);
            this.textBox_leftLifter.TabIndex = 9;
            this.textBox_leftLifter.Text = "0";
            // 
            // button_leftLifterSetup
            // 
            this.button_leftLifterSetup.Location = new System.Drawing.Point(293, 633);
            this.button_leftLifterSetup.Name = "button_leftLifterSetup";
            this.button_leftLifterSetup.Size = new System.Drawing.Size(75, 23);
            this.button_leftLifterSetup.TabIndex = 10;
            this.button_leftLifterSetup.Text = "Установить";
            this.button_leftLifterSetup.UseVisualStyleBackColor = true;
            this.button_leftLifterSetup.Click += new System.EventHandler(this.button_leftLifterSetup_Click);
            // 
            // button_rightLifterSetup
            // 
            this.button_rightLifterSetup.Location = new System.Drawing.Point(655, 633);
            this.button_rightLifterSetup.Name = "button_rightLifterSetup";
            this.button_rightLifterSetup.Size = new System.Drawing.Size(75, 23);
            this.button_rightLifterSetup.TabIndex = 13;
            this.button_rightLifterSetup.Text = "Установить";
            this.button_rightLifterSetup.UseVisualStyleBackColor = true;
            this.button_rightLifterSetup.Click += new System.EventHandler(this.button_rightLifterSetup_Click);
            // 
            // textBox_rightLifter
            // 
            this.textBox_rightLifter.Location = new System.Drawing.Point(549, 635);
            this.textBox_rightLifter.Name = "textBox_rightLifter";
            this.textBox_rightLifter.Size = new System.Drawing.Size(100, 20);
            this.textBox_rightLifter.TabIndex = 12;
            this.textBox_rightLifter.Text = "0";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(374, 638);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(175, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Установка правого подъемника:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button_RL_Off);
            this.groupBox1.Controls.Add(this.button_RL_On);
            this.groupBox1.Location = new System.Drawing.Point(1145, 442);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(137, 80);
            this.groupBox1.TabIndex = 16;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Включение ПП";
            // 
            // button_RL_Off
            // 
            this.button_RL_Off.Location = new System.Drawing.Point(6, 48);
            this.button_RL_Off.Name = "button_RL_Off";
            this.button_RL_Off.Size = new System.Drawing.Size(125, 23);
            this.button_RL_Off.TabIndex = 3;
            this.button_RL_Off.Text = "Выкл";
            this.button_RL_Off.UseVisualStyleBackColor = true;
            this.button_RL_Off.Click += new System.EventHandler(this.button_RL_Off_Click);
            // 
            // button_RL_On
            // 
            this.button_RL_On.Location = new System.Drawing.Point(6, 19);
            this.button_RL_On.Name = "button_RL_On";
            this.button_RL_On.Size = new System.Drawing.Size(125, 23);
            this.button_RL_On.TabIndex = 2;
            this.button_RL_On.Text = "Вкл";
            this.button_RL_On.UseVisualStyleBackColor = true;
            this.button_RL_On.Click += new System.EventHandler(this.button_RL_On_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.button_dirRL_1);
            this.groupBox2.Controls.Add(this.button_dirRL_0);
            this.groupBox2.Location = new System.Drawing.Point(1145, 356);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(137, 80);
            this.groupBox2.TabIndex = 17;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Направление ПП";
            // 
            // button_dirRL_1
            // 
            this.button_dirRL_1.Location = new System.Drawing.Point(6, 48);
            this.button_dirRL_1.Name = "button_dirRL_1";
            this.button_dirRL_1.Size = new System.Drawing.Size(125, 23);
            this.button_dirRL_1.TabIndex = 3;
            this.button_dirRL_1.Text = "1";
            this.button_dirRL_1.UseVisualStyleBackColor = true;
            this.button_dirRL_1.Click += new System.EventHandler(this.button5_Click);
            // 
            // button_dirRL_0
            // 
            this.button_dirRL_0.Location = new System.Drawing.Point(6, 19);
            this.button_dirRL_0.Name = "button_dirRL_0";
            this.button_dirRL_0.Size = new System.Drawing.Size(125, 23);
            this.button_dirRL_0.TabIndex = 2;
            this.button_dirRL_0.Text = "0";
            this.button_dirRL_0.UseVisualStyleBackColor = true;
            this.button_dirRL_0.Click += new System.EventHandler(this.button_dirRL_0_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.button_setupKP);
            this.groupBox3.Controls.Add(this.textBox_setup_KP);
            this.groupBox3.Location = new System.Drawing.Point(1145, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(137, 80);
            this.groupBox3.TabIndex = 18;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Уст. привода КП";
            // 
            // button_setupKP
            // 
            this.button_setupKP.Location = new System.Drawing.Point(6, 48);
            this.button_setupKP.Name = "button_setupKP";
            this.button_setupKP.Size = new System.Drawing.Size(125, 23);
            this.button_setupKP.TabIndex = 1;
            this.button_setupKP.Text = "Установить";
            this.button_setupKP.UseVisualStyleBackColor = true;
            this.button_setupKP.Click += new System.EventHandler(this.button_setupKP_Click);
            // 
            // textBox_setup_KP
            // 
            this.textBox_setup_KP.Location = new System.Drawing.Point(6, 22);
            this.textBox_setup_KP.Name = "textBox_setup_KP";
            this.textBox_setup_KP.Size = new System.Drawing.Size(125, 20);
            this.textBox_setup_KP.TabIndex = 0;
            this.textBox_setup_KP.Text = "0";
            // 
            // groupBox_kp_Direct
            // 
            this.groupBox_kp_Direct.Controls.Add(this.button_dirKP_1);
            this.groupBox_kp_Direct.Controls.Add(this.button_kpDir_0);
            this.groupBox_kp_Direct.Location = new System.Drawing.Point(1145, 98);
            this.groupBox_kp_Direct.Name = "groupBox_kp_Direct";
            this.groupBox_kp_Direct.Size = new System.Drawing.Size(137, 80);
            this.groupBox_kp_Direct.TabIndex = 19;
            this.groupBox_kp_Direct.TabStop = false;
            this.groupBox_kp_Direct.Text = "Направление КП";
            // 
            // button_dirKP_1
            // 
            this.button_dirKP_1.Location = new System.Drawing.Point(6, 48);
            this.button_dirKP_1.Name = "button_dirKP_1";
            this.button_dirKP_1.Size = new System.Drawing.Size(125, 23);
            this.button_dirKP_1.TabIndex = 3;
            this.button_dirKP_1.Text = "1";
            this.button_dirKP_1.UseVisualStyleBackColor = true;
            this.button_dirKP_1.Click += new System.EventHandler(this.button_dirKP_1_Click);
            // 
            // button_kpDir_0
            // 
            this.button_kpDir_0.Location = new System.Drawing.Point(6, 19);
            this.button_kpDir_0.Name = "button_kpDir_0";
            this.button_kpDir_0.Size = new System.Drawing.Size(125, 23);
            this.button_kpDir_0.TabIndex = 2;
            this.button_kpDir_0.Text = "0";
            this.button_kpDir_0.UseVisualStyleBackColor = true;
            this.button_kpDir_0.Click += new System.EventHandler(this.button_kpDir_0_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.button_KP_Off);
            this.groupBox4.Controls.Add(this.button_KP_On);
            this.groupBox4.Location = new System.Drawing.Point(1145, 184);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(137, 80);
            this.groupBox4.TabIndex = 20;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Включение КП";
            // 
            // button_KP_Off
            // 
            this.button_KP_Off.Location = new System.Drawing.Point(6, 48);
            this.button_KP_Off.Name = "button_KP_Off";
            this.button_KP_Off.Size = new System.Drawing.Size(125, 23);
            this.button_KP_Off.TabIndex = 1;
            this.button_KP_Off.Text = "Выкл";
            this.button_KP_Off.UseVisualStyleBackColor = true;
            this.button_KP_Off.Click += new System.EventHandler(this.button_KP_Off_Click);
            // 
            // button_KP_On
            // 
            this.button_KP_On.Location = new System.Drawing.Point(6, 19);
            this.button_KP_On.Name = "button_KP_On";
            this.button_KP_On.Size = new System.Drawing.Size(125, 23);
            this.button_KP_On.TabIndex = 0;
            this.button_KP_On.Text = "Вкл";
            this.button_KP_On.UseVisualStyleBackColor = true;
            this.button_KP_On.Click += new System.EventHandler(this.button_KP_On_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label3);
            this.groupBox5.Controls.Add(this.button_knplp_req);
            this.groupBox5.Location = new System.Drawing.Point(1002, 528);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(137, 66);
            this.groupBox5.TabIndex = 21;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "КНПЛП";
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(9, 45);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(122, 18);
            this.label3.TabIndex = 1;
            this.label3.Text = "0";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // button_knplp_req
            // 
            this.button_knplp_req.Location = new System.Drawing.Point(6, 19);
            this.button_knplp_req.Name = "button_knplp_req";
            this.button_knplp_req.Size = new System.Drawing.Size(125, 23);
            this.button_knplp_req.TabIndex = 0;
            this.button_knplp_req.Text = "Запрос";
            this.button_knplp_req.UseVisualStyleBackColor = true;
            this.button_knplp_req.Click += new System.EventHandler(this.button_knplp_0_Click);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.label4);
            this.groupBox6.Controls.Add(this.button_knppp_req);
            this.groupBox6.Location = new System.Drawing.Point(1145, 528);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(137, 66);
            this.groupBox6.TabIndex = 22;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "КНППП";
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(6, 45);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(122, 18);
            this.label4.TabIndex = 3;
            this.label4.Text = "0";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // button_knppp_req
            // 
            this.button_knppp_req.Location = new System.Drawing.Point(6, 18);
            this.button_knppp_req.Name = "button_knppp_req";
            this.button_knppp_req.Size = new System.Drawing.Size(125, 23);
            this.button_knppp_req.TabIndex = 2;
            this.button_knppp_req.Text = "Запрос";
            this.button_knppp_req.UseVisualStyleBackColor = true;
            this.button_knppp_req.Click += new System.EventHandler(this.button_knppp_req_Click);
            // 
            // button18
            // 
            this.button18.Location = new System.Drawing.Point(1145, 600);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(137, 29);
            this.button18.TabIndex = 23;
            this.button18.Text = "Старт";
            this.button18.UseVisualStyleBackColor = true;
            this.button18.Click += new System.EventHandler(this.button18_Click);
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.button_liftNull);
            this.groupBox7.Controls.Add(this.button_stopRotation);
            this.groupBox7.Controls.Add(this.button_stopLift);
            this.groupBox7.Controls.Add(this.button_step5);
            this.groupBox7.Controls.Add(this.button_step4);
            this.groupBox7.Controls.Add(this.button_step3);
            this.groupBox7.Controls.Add(this.button_step2);
            this.groupBox7.Controls.Add(this.button_step1);
            this.groupBox7.Location = new System.Drawing.Point(1288, 12);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(150, 329);
            this.groupBox7.TabIndex = 24;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Автоматический режим";
            // 
            // button_liftNull
            // 
            this.button_liftNull.Location = new System.Drawing.Point(6, 277);
            this.button_liftNull.Name = "button_liftNull";
            this.button_liftNull.Size = new System.Drawing.Size(138, 46);
            this.button_liftNull.TabIndex = 7;
            this.button_liftNull.Text = "Нулевая позиция";
            this.button_liftNull.UseVisualStyleBackColor = true;
            this.button_liftNull.Click += new System.EventHandler(this.button_liftNull_Click);
            // 
            // button_stopRotation
            // 
            this.button_stopRotation.Location = new System.Drawing.Point(6, 220);
            this.button_stopRotation.Name = "button_stopRotation";
            this.button_stopRotation.Size = new System.Drawing.Size(138, 51);
            this.button_stopRotation.TabIndex = 6;
            this.button_stopRotation.Text = "Остановить вращение";
            this.button_stopRotation.UseVisualStyleBackColor = true;
            // 
            // button_stopLift
            // 
            this.button_stopLift.Location = new System.Drawing.Point(6, 163);
            this.button_stopLift.Name = "button_stopLift";
            this.button_stopLift.Size = new System.Drawing.Size(138, 51);
            this.button_stopLift.TabIndex = 5;
            this.button_stopLift.Text = "Остановить подъем";
            this.button_stopLift.UseVisualStyleBackColor = true;
            this.button_stopLift.Click += new System.EventHandler(this.button_stopLift_Click);
            // 
            // button_step5
            // 
            this.button_step5.Location = new System.Drawing.Point(6, 134);
            this.button_step5.Name = "button_step5";
            this.button_step5.Size = new System.Drawing.Size(138, 23);
            this.button_step5.TabIndex = 4;
            this.button_step5.Text = "Step 5";
            this.button_step5.UseVisualStyleBackColor = true;
            this.button_step5.Click += new System.EventHandler(this.button_step5_Click);
            // 
            // button_step4
            // 
            this.button_step4.Location = new System.Drawing.Point(6, 105);
            this.button_step4.Name = "button_step4";
            this.button_step4.Size = new System.Drawing.Size(138, 23);
            this.button_step4.TabIndex = 3;
            this.button_step4.Text = "Step 4";
            this.button_step4.UseVisualStyleBackColor = true;
            this.button_step4.Click += new System.EventHandler(this.button_step4_Click);
            // 
            // button_step3
            // 
            this.button_step3.Location = new System.Drawing.Point(6, 77);
            this.button_step3.Name = "button_step3";
            this.button_step3.Size = new System.Drawing.Size(138, 23);
            this.button_step3.TabIndex = 2;
            this.button_step3.Text = "Step 3";
            this.button_step3.UseVisualStyleBackColor = true;
            this.button_step3.Click += new System.EventHandler(this.button_step3_Click);
            // 
            // button_step2
            // 
            this.button_step2.Location = new System.Drawing.Point(6, 48);
            this.button_step2.Name = "button_step2";
            this.button_step2.Size = new System.Drawing.Size(138, 23);
            this.button_step2.TabIndex = 1;
            this.button_step2.Text = "Step 2";
            this.button_step2.UseVisualStyleBackColor = true;
            this.button_step2.Click += new System.EventHandler(this.button_step2_Click);
            // 
            // button_step1
            // 
            this.button_step1.Location = new System.Drawing.Point(6, 19);
            this.button_step1.Name = "button_step1";
            this.button_step1.Size = new System.Drawing.Size(138, 23);
            this.button_step1.TabIndex = 0;
            this.button_step1.Text = "Step 1";
            this.button_step1.UseVisualStyleBackColor = true;
            this.button_step1.Click += new System.EventHandler(this.button_step1_Click);
            // 
            // button_upAll
            // 
            this.button_upAll.Location = new System.Drawing.Point(1294, 347);
            this.button_upAll.Name = "button_upAll";
            this.button_upAll.Size = new System.Drawing.Size(138, 51);
            this.button_upAll.TabIndex = 25;
            this.button_upAll.Text = "Подъем";
            this.button_upAll.UseVisualStyleBackColor = true;
            this.button_upAll.Click += new System.EventHandler(this.button_upAll_Click);
            // 
            // label_sensVal
            // 
            this.label_sensVal.AutoSize = true;
            this.label_sensVal.Location = new System.Drawing.Point(1344, 409);
            this.label_sensVal.Name = "label_sensVal";
            this.label_sensVal.Size = new System.Drawing.Size(35, 13);
            this.label_sensVal.TabIndex = 26;
            this.label_sensVal.Text = "label5";
            // 
            // DebugForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1514, 660);
            this.Controls.Add(this.label_sensVal);
            this.Controls.Add(this.button_upAll);
            this.Controls.Add(this.groupBox7);
            this.Controls.Add(this.button18);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox_kp_Direct);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox_SwitchLeftLifter);
            this.Controls.Add(this.groupBox_directionLeftLifter);
            this.Controls.Add(this.button_rightLifterSetup);
            this.Controls.Add(this.textBox_rightLifter);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.button_leftLifterSetup);
            this.Controls.Add(this.textBox_leftLifter);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox_p5);
            this.Controls.Add(this.groupBox_p4);
            this.Controls.Add(this.groupBox_P3);
            this.Controls.Add(this.gb_r2);
            this.Controls.Add(this.panel1);
            this.Name = "DebugForm";
            this.Text = "DebugForm";
            this.panel1.ResumeLayout(false);
            this.groupBox_SwitchLeftLifter.ResumeLayout(false);
            this.groupBox_directionLeftLifter.ResumeLayout(false);
            this.gb_r2.ResumeLayout(false);
            this.groupBox_P3.ResumeLayout(false);
            this.groupBox_p4.ResumeLayout(false);
            this.groupBox_p5.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox_kp_Direct.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button_unclenchPenola;
        private System.Windows.Forms.Button button_pinchPenola;
        private System.Windows.Forms.Button button_r2_down;
        private System.Windows.Forms.Button button_r2_up;
        private System.Windows.Forms.GroupBox gb_r2;
        private System.Windows.Forms.GroupBox groupBox_P3;
        private System.Windows.Forms.Button button_r3_up;
        private System.Windows.Forms.Button button_p3_down;
        private System.Windows.Forms.GroupBox groupBox_directionLeftLifter;
        private System.Windows.Forms.GroupBox groupBox_p4;
        private System.Windows.Forms.Button button_p4_up;
        private System.Windows.Forms.Button button_p4_down;
        private System.Windows.Forms.GroupBox groupBox_p5;
        private System.Windows.Forms.Button button_p5_up;
        private System.Windows.Forms.Button button_p5_down;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox_leftLifter;
        private System.Windows.Forms.Button button_leftLifterSetup;
        private System.Windows.Forms.Button button_rightLifterSetup;
        private System.Windows.Forms.TextBox textBox_rightLifter;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox_SwitchLeftLifter;
        private System.Windows.Forms.Button button_dirLL_1;
        private System.Windows.Forms.Button button_dirLL_0;
        private System.Windows.Forms.Button button_LL_Off;
        private System.Windows.Forms.Button button_LL_On;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button_RL_Off;
        private System.Windows.Forms.Button button_RL_On;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button button_dirRL_1;
        private System.Windows.Forms.Button button_dirRL_0;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button button_setupKP;
        private System.Windows.Forms.TextBox textBox_setup_KP;
        private System.Windows.Forms.GroupBox groupBox_kp_Direct;
        private System.Windows.Forms.Button button_dirKP_1;
        private System.Windows.Forms.Button button_kpDir_0;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button button_KP_Off;
        private System.Windows.Forms.Button button_KP_On;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button button_knplp_req;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button button_knppp_req;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Button button_step3;
        private System.Windows.Forms.Button button_step2;
        private System.Windows.Forms.Button button_step1;
        private System.Windows.Forms.Button button_step4;
        private System.Windows.Forms.Button button_step5;
        private System.Windows.Forms.Button button_stopLift;
        private System.Windows.Forms.Button button_stopRotation;
        private System.Windows.Forms.Button button_liftNull;
        private System.Windows.Forms.Button button_upAll;
        private System.Windows.Forms.Label label_sensVal;
    }
}