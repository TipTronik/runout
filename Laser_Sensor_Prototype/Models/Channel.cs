﻿using System.IO.Ports;

namespace Laser_Sensor_Prototype
{
    public class Channel
    {
        public int channel;
        public SerialPort serialPort = new SerialPort();

        public Channel(SerialPort serialPort, int channel)
        {
            this.serialPort = serialPort;
            this.channel = channel;
        }
    }
}